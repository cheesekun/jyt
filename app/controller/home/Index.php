<?php
namespace app\controller\home;

use app\BaseController;
use app\model\home\IndexModel;
use think\Request;

class Index extends BaseController {

	//======================================================================
	// PUBLIC FUNCTIONS
	//======================================================================

    /**
     * index 首页列表
     * 
     * @param $request
	 * @return void
     */
    public function index(Request $request) {

        // 常用功能显示数量
        $num_configs = 10;
        // 最新资讯显示数量
        $num_news = 10;
        // 通知公告显示数量
        $num_notices = 10;

        $db = new IndexModel();
        $list = array();

        $data_redis = $request->middleware('data_redis');
        $list['list_configs'] = $db->getConfigs($num_configs, $data_redis['userid'])->toArray();

        foreach ($list['list_configs'] as $key => $value) {
            $list_menu = $db->getMenus($value['pid']);
            $list['list_configs'][$key]['p_title'] = $list_menu['title'];
            $list['list_configs'][$key]['path'] = $list_menu['path'] . '/' . $value['path'];
        }

        $list['list_news'] = $db->getNews($num_news)->toArray();
        $list['list_notices'] = $db->getNotices($num_notices)->toArray();

        $data['code'] = 200;
        $data['data']['list'] = $list;

        return json($data);
    }
}
