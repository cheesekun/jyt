<?php
namespace app\controller\system;

use app\BaseController;
use app\model\system\OperationModel;

/**
 * Operation
 */
class Operation extends BaseController {

	//======================================================================
	// PUBLIC FUNCTIONS
	//======================================================================

    /**
     * index 操作日志列表
     * 
	 * @return void
     */
    public function index() {
        $page_size = isset($_GET['pageSize']) ? $_GET['pageSize'] : '';
        $page_index = isset($_GET['pageIndex']) ? $_GET['pageIndex'] : '';

        // 过滤条件：操作人、操作模块、操作开始时间、操作结束时间
        $filters = [
            'filter_user_name' => isset($_GET['filterUserName']) ? $_GET['filterUserName'] : '',
            'filter_module' => isset($_GET['filterModule']) ? $_GET['filterModule'] : '',
            'filter_time_start' => isset($_GET['filteTimeStart']) ? $_GET['filteTimeStart'] : '',
            'filter_time_end' => isset($_GET['filteTimeEnd']) ? $_GET['filteTimeEnd'] : ''
        ];

        $db = new OperationModel();
        $list = $db->getOperations($page_size, $page_index, $filters)->toArray();

        $data['code'] = 200;
        $data['data']['list'] = $list['data'];
        $data['data']['total'] = $list['total'];

        return json($data);
    }
}