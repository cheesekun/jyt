<?php
namespace app\controller\system;

use app\BaseController;
use think\exception\ValidateException;
use app\model\system\FileModel;
use think\Request;

/**
 * File
 */
class File extends BaseController {

	//======================================================================
	// PUBLIC FUNCTIONS
	//======================================================================

    /**
     * index 操作日志列表
     * 
	 * @return void
     */
    public function index() {
        $page_size = isset($_GET['pageSize']) ? $_GET['pageSize'] : '';
        $page_index = isset($_GET['pageIndex']) ? $_GET['pageIndex'] : '';

        $db = new FileModel();
        $module_list = $db->getModules()->toArray();

        $modules = [];
        foreach ($module_list as $key => $value) {
            $modules[$key] = $value['title'];
        }

        // 过滤条件：操作模块、操作人、操作开始时间、操作结束时间
        $filters = [
            'filter_user_name' => isset($_GET['filterUserName']) ? $_GET['filterUserName'] : '',
            'filter_module' => isset($_GET['filterModule']) ? $_GET['filterModule'] : '',
            'filter_time_start' => isset($_GET['filteTimeStart']) ? $_GET['filteTimeStart'] : '',
            'filter_time_end' => isset($_GET['filteTimeEnd']) ? $_GET['filteTimeEnd'] : '',
            'modules' => $modules
        ];

        $list = $db->getFiles($page_size, $page_index, $filters)->toArray();

        $data['code'] = 200;
        $data['data']['list'] = $list['data'];
        $data['data']['modules'] = $module_list;
        $data['data']['total'] = $list['total'];

        return json($data);
    }

    /**
     * upload 上传文件
     * 
     * @param $request
	 * @return void
     */
    public function upload(Request $request){
        $data_redis = $request->middleware('data_redis');
        $file = $request->file();

        try{
            // 验证文件大小格式
            validate(['file'=>'fileSize:1048576|fileExt:png,jpg,doc,docx,pdf,xls,xlsx'])
            ->check($file);
 
            $savename = \think\facade\Filesystem::disk('public')->putFile('uploads', $file['file']);
            $url = \think\facade\Filesystem::getDiskConfig('public', 'url') . '/' . str_replace('\\', '/', $savename);
            $data['virtual_id'] = isset($_POST['virtual_id']) ? $_POST['virtual_id'] : NULL;
            $data['module'] = isset($_POST['module']) ? $_POST['module'] : NULL;
            $data['file_path'] = $url;
            $data['source_name'] = $_FILES['file']['name'];
            $data['file_size'] = $_FILES['file']['size'];
            $data['create_by'] = $data_redis['userid'];
            $data['modify_by'] = $data_redis['userid'];
            $data['create_time'] = date('Y-m-d H:i:s');
            $data['modify_time'] = date('Y-m-d H:i:s');

            $db = new FileModel();
            $add_id = $db->addProductFile($data);

            if ($add_id) {
                $datasmg['code'] = 200;
                $datasmg['id'] = (int)$add_id;
                $datasmg['name'] = $data['source_name'];
                $datasmg['msg'] = "上传成功"; // 前端要求， mseeage -> msg，不提示弹框
            } else {
                $datasmg['code'] = 404; // 前端要求，上传失败 404 -> 200，不提示弹框
                $datasmg['name'] = $data['source_name']; // 前端要求，上传失败返回文件名
                $datasmg['msg'] = "上传失败";
            }

            return json($datasmg);
        }catch(ValidateException $e){
            $datasmg['code'] = 200; // 前端要求，验证失败 404 -> 200，不提示弹框
            $datasmg['name'] = $_FILES['file']['name']; // 前端要求，验证失败返回文件名
            $datasmg['msg'] = $e->getError();

            return json($datasmg);
        }
    }

    /**
     * download 文件下载
     * 
	 * @return void
     */
    public function download() {
        $id = $_GET['id'];

        $db = new FileModel();
        $list = $db->getFile($id);

        $filePath = '.' . $list['file_path'];
        $fileName = $list['source_name'];

        return download($filePath, $fileName)->expire(300);
    }
}