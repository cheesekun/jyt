<?php
namespace app\controller\system;

use app\BaseController;
use app\validate\UserValidate;
use think\exception\ValidateException;
use app\model\system\UserModel;
use app\model\system\OperationModel;
use think\Request;

/**
 * User
 */
class User extends BaseController {

    //======================================================================
    // PUBLIC FUNCTIONS
    //======================================================================

    /**
     * index 用户列表
     * 
     * @return void
     */
    public function index() {
        $page_size = isset($_GET['pageSize']) ? $_GET['pageSize'] : '';
        $page_index = isset($_GET['pageIndex']) ? $_GET['pageIndex'] : '';

        // 过滤条件：用户姓名、手机号、状态
        $filters = [
            'filter_user_name' => isset($_GET['filterUserName']) ? $_GET['filterUserName'] : '',
            'filter_telephtone' => isset($_GET['filterTelephone']) ? $_GET['filterTelephone'] : '',
            'filter_user_state' => isset($_GET['filterState']) ? $_GET['filterState'] : ''
        ];

        $db = new UserModel();
        $list = $db->getUsers($page_size, $page_index, $filters)->toArray();

        // 用户角色
        $roles = array();
        foreach ($list['data'] as $key => $value) {
            $roles = explode(',', $value['role_id']);

            if (count($roles) > 0) {
                foreach ($roles as $roles_key => $roles_value) {
                    $role = $db->getRole($roles_value);

                    if ($role) {
                        $list['data'][$key]['roles'][$roles_key]['id'] = $roles_value;
                        $list['data'][$key]['roles'][$roles_key]['name'] = $role['title'];
                    }

                }
            }
        }

        $role_array = $db->getRoles()->toArray();
        $role_list = array();

        foreach ($role_array as $key => $value) {
            $role_list[$key]['id'] = (string)$value['id'];
            $role_list[$key]['name'] = $value['title'];
        }

        $data['code'] = 200;
        $data['data']['list'] = $list['data'];
        $data['data']['roles'] = $role_list;
        $data['data']['total'] = $list['total'];

        return json($data);
    }

    /**
     * add 用户添加
     * 
     * @param $request
     * @return void
     */
    public function add(Request $request) {

        if (request()->isPost() && $this->validateForm() === true) {
            $data['username'] = $_POST['username'];
            $data['telephone'] = $_POST['telephone'];

            $db = new UserModel();
            $list_tel = $db->getUserByTelephone($data['telephone'])->toArray();

            if (count($list_tel) > 0) {
                $datasmg['code'] = 404;
                $datasmg['message'] = '手机号已注册';

                return json($datasmg);
            }

            $list_name = $db->getUserByName($data['username'])->toArray();

            if (count($list_name) > 0) {
                $datasmg['code'] = 404;
                $datasmg['message'] = '用户名已注册';

                return json($datasmg);
            }

            $data_redis = $request->middleware('data_redis');

            $data['password'] = md5($_POST['password'] . 'jieyitan');
            $roles = explode(',', $_POST['role_id']);
            $data['role_id'] = implode(',', array_filter(array_unique($roles)));
            $data['create_by'] = $data_redis['userid'];
            $data['state'] = '1';
            $data['create_time'] = date('Y-m-d H:i:s');

            // 添加操作日志
            $data_log['user_id'] = $data['create_by'];
            $data_log['menu_id'] = '4';// $_POST['menu_id']; // T.B.D 前端未传先注释
            $data_log['time'] = $data['create_time'];
            $data_log['url'] = 'user/add';// $_POST['url']; // T.B.D 前端未传先注释
            $data_log['log'] = '添加用户：' . $data['username'];

            $db = new OperationModel();
            $db->addOperation($data_log);

            $db = new UserModel();
            $add = $db->addUser($data);

            if ($add) {
                $datasmg['code'] = 200;
                $datasmg['message'] = "添加成功";
            } else {
                $datasmg['code'] = 404;
                $datasmg['message'] = "添加失败";
            }

        } else {
            $datasmg['code'] = 404;
            $datasmg['message'] = $this->validateForm();
        }
        
        return json($datasmg);
    }

    /**
     * state 更新用户状态
     * 
     * @param $request
     * @return void
     */
    public function state(Request $request) {

        if (request()->isPost()) {
            $data_redis = $request->middleware('data_redis');

            $data['id'] = $_POST['id'];
            $data['modify_by'] = $data_redis['userid'];
            $data['username'] = $_POST['username'];
            $data['state'] = $_POST['state'];
            $data['modify_time'] = date('Y-m-d H:i:s');

            // 添加操作日志
            $data_log['user_id'] = $data['modify_by'];
            $data_log['menu_id'] = '4';// $_POST['menu_id']; // T.B.D 前端未传先注释
            $data_log['time'] = $data['modify_time'];
            $data_log['url'] = 'useruser/state';// $_POST['url']; // T.B.D 前端未传先注释
            $data_log['log'] = '更新用户状态：' . $data['username'];

            $db = new OperationModel();
            $db->addOperation($data_log);

            $db = new UserModel();
            $edit = $db->updateUser($data);

            if ($edit) {
                $datasmg['code'] = 200;
                $datasmg['message'] = "修改成功";
            } else {
                $datasmg['code'] = 404;
                $datasmg['message'] = "修改失败";
            }

        } else {
            $datasmg['code'] = 404;
            $datasmg['message'] = "修改失败";
        }

        return json($datasmg);
    }

    /**
     * see 查看用户详情
     * 
     * @return void
     */
    public function see() {
        $id = $_GET['id'];

        $db = new UserModel();
        $list = $db->seeUser($id);

        $roles = explode(',', $list['role_id']);

        if (count($roles) > 0) {
            foreach ($roles as $roles_key => $roles_value) {
                $role = $db->getRole($roles_value);

                if ($role) {
                    $list['roles'][$roles_key]['id'] = $roles_value;
                    $list['roles'][$roles_key]['name'] = $role['title'];
                }
            }
        }

        $data['code'] = 200;
        $data['data'] = $list;

        return json($data);
    }

    /**
     * role 更新用户角色
     * 
     * @param $request
     * @return void
     */
    public function role(Request $request) {

        if (request()->isPost()) {
            $data_redis = $request->middleware('data_redis');

            $data['id'] = $_POST['id'];
            $data['role_id'] = $_POST['role_id'];
            $data['modify_by'] = $data_redis['userid'];
            $data['username'] = $_POST['username'];
            $data['modify_time'] = date('Y-m-d H:i:s');

            // 添加操作日志
            $data_log['user_id'] = $data['modify_by'];
            $data_log['menu_id'] = '4';// $_POST['menu_id']; // T.B.D 前端未传先注释
            $data_log['time'] = $data['modify_time'];
            $data_log['url'] = 'useruser/role';// $_POST['url']; // T.B.D 前端未传先注释
            $data_log['log'] = '更新用户角色：' . $data['username'];

            $db = new OperationModel();
            $db->addOperation($data_log);

            $db = new UserModel();
            $edit = $db->updateUser($data);

            if ($edit) {
                $datasmg['code'] = 200;
                $datasmg['message'] = "修改成功";
            } else {
                $datasmg['code'] = 404;
                $datasmg['message'] = "修改失败";
            }

        } else {
            $datasmg['code'] = 404;
            $datasmg['message'] = "修改失败";
        }

        return json($datasmg);
    }

    /**
     * group 更新用户组织
     * 
     * @param $request
     * @return void
     */
    public function group(Request $request) {

        if (request()->isPost()) {
            $data_redis = $request->middleware('data_redis');

            $data['id'] = $_POST['id'];
            $data['organization_id'] = $_POST['organization_id'];
            $data['modify_by'] = $data_redis['userid'];
            $data['username'] = $_POST['username'];
            $data['modify_time'] = date('Y-m-d H:i:s');

            // 添加操作日志
            $data_log['user_id'] = $data['modify_by'];
            $data_log['menu_id'] = '4';// $_POST['menu_id']; // T.B.D 前端未传先注释
            $data_log['time'] = $data['modify_time'];
            $data_log['url'] = 'useruser/group';// $_POST['url']; // T.B.D 前端未传先注释
            $data_log['log'] = '更新用户组织：' . $data['username'];

            $db = new OperationModel();
            $db->addOperation($data_log);

            $db = new UserModel();
            $edit = $db->updateUser($data);

            if ($edit) {
                $datasmg['code'] = 200;
                $datasmg['message'] = "修改成功";
            } else {
                $datasmg['code'] = 404;
                $datasmg['message'] = "修改失败";
            }

        } else {
            $datasmg['code'] = 404;
            $datasmg['message'] = "修改失败";
        }

        return json($datasmg);
    }

    //======================================================================
    // PROTECTED FUNCTIONS
    //======================================================================

    /**
     * validateForm 验证
     * 
     * @return void
     */
    protected function validateForm() {
        $data = request()->param();

        try {
            validate(UserValidate::class)->check($data);

            return  true;
        } catch (ValidateException $e) {
            
            return $e->getError();
        }
    }
}