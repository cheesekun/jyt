<?php
declare (strict_types = 1);

namespace app\controller\admin;

use app\BaseController;
use app\validate\UnitValidate;
use think\exception\ValidateException;
use app\model\admin\UnitModel;
use app\model\system\OperationModel;
use think\Request;

class Unit extends BaseController {

	//======================================================================
	// PUBLIC FUNCTIONS
	//======================================================================

    /**
     * index 单位列表
     * 
	 * @return void
     */
    public function index() {
        $page_size = isset($_GET['pageSize']) ? $_GET['pageSize'] : '';
        $page_index = isset($_GET['pageIndex']) ? $_GET['pageIndex'] : '';

        // 过滤条件：单位分类、单位名称
        $filters = [
            'filter_unit_type' => isset($_GET['filterUnitType']) ? $_GET['filterUnitType'] : '',
            'filter_unit_name' => isset($_GET['filterUnitName']) ? $_GET['filterUnitName'] : ''
        ];

        $db = new UnitModel();
        $list = $db->getUnits($page_size, $page_index, $filters)->toArray();
        $unit_types = $db->getUnitType()->toArray();

        foreach ($list['data'] as $key => $value) {
            $unit = $db->findUnit($value['id']);
            $list['data'][$key]['base_unit'] = $unit['base_unit'];
        }

        $data['code'] = 200;
        $data['data']['list'] = $list['data'];
        $data['data']['unit_types'] = $unit_types;
        $data['data']['total'] = $list['total'];

        return json($data);
    }

    /**
     * see 单位数组
     * 
	 * @return void
     */
    public function see() {
        $type = isset($_GET['type']) ? $_GET['type'] : '';

        $db = new UnitModel();
        $list = $db->getUnit($type)->toArray();

        $array = array();
        $dictionaries = array();

        foreach ($list as $key => $value) {
            if (!in_array($value['type_id'], $array)) {
                array_push($array, $value['type_id']);
                $dictionaries[$key]['id'] = $value['type_id'];
                $dictionaries[$key]['unit_type'] = $value['type'];
                $dictionaries[$key]['units'] = $this->initDatas($list, $value['type_id']);
            }
        }

        $data['code'] = 200;
        $data['data']['list'] = array_values($dictionaries);

        return json($data);
    }

    /**
     * add 添加单位
     * 
	 * @return void
     */
    public function add(Request $request) {

        if (request()->isPost() && $this->validateForm() === true) {
            $data_redis = $request->middleware('data_redis');

            $data['name'] = $_POST['name'];
            $data['type_id'] = $_POST['type_id'];
            $data['is_base'] = $_POST['is_base'];
            $data['conversion_ratio'] = $_POST['conversion_ratio'];
            $data['create_by'] = $data_redis['userid'];
            $data['modify_by'] = $data_redis['userid'];
            $data['create_time'] = date('Y-m-d H:i:s');
            $data['modify_time'] = date('Y-m-d H:i:s');

            // 添加操作日志
            $data_log['user_id'] = $data['create_by'];
            $data_log['menu_id'] = '58';// $_POST['menu_id']; // T.B.D 前端未传先注释
            $data_log['time'] = $data['create_time'];
            $data_log['url'] = 'unit/add';// $_POST['url']; // T.B.D 前端未传先注释
            $data_log['log'] = '新建单位：' . $data['name'];

            $db = new OperationModel();
            $db->addOperation($data_log);

            $db = new UnitModel();
            $add = $db->addUnit($data);

            if ($add) {
                $datasmg['code'] = 200;
                $datasmg['message'] = "添加成功";
            } else {
                $datasmg['code'] = 404;
                $datasmg['message'] = "添加失败";
            }

        } else {
            $datasmg['code'] = 404;
            $datasmg['message'] = $this->validateForm();
        }

        return json($datasmg);
    }

    /**
     * edit 修改单位
     * 
	 * @return void
     */
    public function edit(Request $request) {

        if (request()->isPost() && $this->validateForm() === true) {
            $data_redis = $request->middleware('data_redis');

            $data['id'] = $_POST['id'];
            $data['conversion_ratio'] = $_POST['conversion_ratio'];
            $data['modify_by'] = $data_redis['userid'];
            $data['modify_time'] = date('Y-m-d H:i:s');

            // 添加操作日志
            $data_log['user_id'] = $data['modify_by'];
            $data_log['menu_id'] = '58';// $_POST['menu_id']; // T.B.D 前端未传先注释
            $data_log['time'] = $data['modify_time'];
            $data_log['url'] = 'unit/edit';// $_POST['url']; // T.B.D 前端未传先注释
            $data_log['log'] = '编辑单位';

            $db = new OperationModel();
            $db->addOperation($data_log);

            $db = new UnitModel();
            $edit = $db->editUnit($data);

            if ($edit) {
                $datasmg['code'] = 200;
                $datasmg['message'] = "修改成功";
            } else {
                $datasmg['code'] = 404;
                $datasmg['message'] = "修改失败";
            }

        } else {
            $datasmg['code'] = 404;
            $datasmg['message'] = $this->validateForm();
        }

        return json($datasmg);
    }

    /**
     * del 删除单位
     * 
	 * @return void
     */
    public function del(Request $request) {
        $data_redis = $request->middleware('data_redis');

        $id = $_POST['id'];

        // 添加操作日志
        $data['user_id'] = $data_redis['userid'];
        $data_log['menu_id'] = '58';// $_POST['menu_id']; // T.B.D 前端未传先注释
        $data_log['time'] = date('Y-m-d H:i:s');
        $data_log['url'] = 'unit/del';// $_POST['url']; // T.B.D 前端未传先注释
        $data_log['log'] = '删除单位';

        $db = new OperationModel();
        $db->addOperation($data_log);

        $db = new UnitModel();
        $del = $db->delUnit($id);

        if($del){
            $datasmg['code'] = 200;
            $datasmg['message'] = "删除成功";
        }else{
            $datasmg['code'] = 404;
            $datasmg['message'] = "删除失败";
        }

        return json($datasmg);
    }

    /**
     * find 查看单位详情
     * 
	 * @return void
     */
    public function find() {
        $id = $_GET['id'];

        $db = new UnitModel();
        $list = $db->findUnit($id);

        $data['code'] = 200;
        $data['data'] = $list;

        return json($data);
    }

    /**
     * initDatas
     * 
     * @param $list
     * @param $type_id
	 * @return $array
     */
    private function initDatas($list, $type_id = '') {
        foreach ($list as $value) {
            if ($type_id == $value['type_id']) {
                $array[] = [
                    'unit_id'             => $value['id'],
                    'name'                => $value['name'],
                    'conversion_ratio'    => $value['conversion_ratio']
                ];
            }
        }

        return $array;
    }

	//======================================================================
	// PROTECTED FUNCTIONS
	//======================================================================

    /**
     * validateForm 验证
     * 
	 * @return void
     */
    protected function validateForm() {
        $data = request()->param();

        try {
            validate(UnitValidate::class)->check($data);

            return  true;
        } catch (ValidateException $e) {
            
            return $e->getError();
        }
    }
}
