<?php
namespace app\controller\product;

use app\BaseController;
use app\validate\ProductDataValidate;
use app\validate\ProductDataTransportValidate;
use think\exception\ValidateException;
use app\model\product\ProductCalculateModel;
use app\model\product\ProductDataModel;
use app\model\product\ProductMaterialModel;
use app\model\system\OperationModel;
use think\Request;

/**
 * ProductData
 */
class ProductData extends BaseController {

	//======================================================================
	// PUBLIC FUNCTIONS
	//======================================================================

    /**
     * index 产品核算数据管理列表
     * 
	 * @return void
     */
    public function index() {
        $id = $_GET['id']; //产品核算id
        $product_id = $_GET['product_id'];
        $data_stage = $_GET['data_stage'];
        $page_size = isset($_GET['pageSize']) ? $_GET['pageSize'] : '';
        $page_index = isset($_GET['pageIndex']) ? $_GET['pageIndex'] : '';
        $transport_ids = [8,12,14,16,18];


        // 数据管理（结果预览）
        if ($data_stage == '0') {
            $db = new ProductCalculateModel();
            $list = $db->getCalculate($id);
            $list_stage = explode(',', $list['stage']);
            $list_all = $db->seeCalculateData($id);
    
            $array = array();
            foreach ($list_stage as $list_key => $list_value) {
                $db = new ProductCalculateModel();
                $name = $db->getStageName($list_key + 1);
                foreach ($list_all as $key => $value) {
                    if ($value['data_stage'] == $list_value) {
                        // 匹配类型：1-因子，2-产品
                        $db = new ProductDataModel();
                        if ($value['match_type'] == 1 && $value['match_id']) {
                            $match = $db->getFactorsByMatchType($value['match_id']);
                            $value['match_unit'] = $match['molecule'] . '/' . $match['denominator'];
                        } elseif ($value['match_type'] == 2 && $value['match_id']) {
                            $match = $db->getProductCalculatesByMatchType($value['match_id']);
                            $value['match_unit'] = 'kgCO2e/' . $match['unit_str'];
                        }

                        if (in_array($value['category_id'], $transport_ids)) {
                            $value['unit_str'] = $value['unit_str'] . 'km';
                        }

                        $array[$list_key]['data_stage'] = $name['name'];
                        $array[$list_key]['datas'][] = $value;
                    }
                }
            }

            $data['code'] = 200;
            $data['data']['list'] = $array;
            $data['data']['total_emissions'] = $list['emissions'];
        } else {
            $db = new ProductDataModel();
            $list = $db->getDataManagements($id, $product_id, $data_stage, $page_size, $page_index)->toArray();

            foreach ($list['data'] as $key => $value) {
                // 匹配类型：1-因子，2-产品
                if ($value['match_type'] == 1 && $value['match_id']) {
                    $match = $db->getFactorsByMatchType($value['match_id']);
                    $list['data'][$key]['match_name'] = $match['title'];
                    $list['data'][$key]['match_unit'] = $match['molecule'] . '/' . $match['denominator'];
                } elseif ($value['match_type'] == 2 && $value['match_id']) {
                    $match = $db->getProductCalculatesByMatchType($value['match_id']);
                    $list['data'][$key]['match_name'] = $match['product_name'];
                    $list['data'][$key]['match_unit'] = 'kgCO2e/' . $match['unit_str'];
                }

                $category = $db->getStageName($value['category']);
                $list['data'][$key]['category_id'] = $value['category'];
                $list['data'][$key]['category'] = $category['name'];

                if (in_array($value['category'], $transport_ids)) {
                    $list['data'][$key]['unit_str'] = $value['unit_str'] . 'km';
                }

                $array = array();

                if (!empty($value['materials'])) {
                    $materials = explode(',', $value['materials']);
                    foreach ($materials as $materials_key => $materials_value) {
                        $array[$materials_key]['name'] = $materials_value;
                    }

                    $numbers = explode(',', $value['numbers']);
                    foreach ($numbers as $numbers_key => $numbers_value) {
                        $array[$numbers_key]['number'] = $numbers_value;
                    }

                    $units = explode(',', $value['units']);
                    foreach ($units as $units_key => $units_value) {
                        $array[$units_key]['unit'] = $units_value;
                    }

                    $list['data'][$key]['materials'] = $array;
                }
            }

            // 获取阶段下的类别
            $data_categories = $db->getCategoryName($data_stage)->toArray();

            // 获取阶段下的材料
            $data_materials = $db->getDataManagementByStage($data_stage, $id)->toArray();
            $data_product = $db->seeProduct($product_id);

            // 剔除材料空值
            foreach ($data_materials as $key => $value) {
                if (empty($value['material']) || in_array($value['material'], $data_materials)) {
                    unset($data_materials[$key]);
                }
            }

            array_unshift($data_materials, $data_product);

            // 获取供应商
            $db = new ProductMaterialModel();
            $data_suppliers = $db->getSuppliers()->toArray();

            $db = new ProductCalculateModel();
            $list_stage = $db->getCalculate($id);
            $stage_list = explode(',', $list_stage['stage']);

            // 获取阶段tab
            $data_stages = array();
            foreach ($stage_list as $key => $value) {
                $name = $db->getStageName($value);
                $data_stages[$value]['id'] = $value;
                $data_stages[$value]['name'] = $name['name'];
            }

            // 按升序排列阶段
            ksort($data_stages);

            array_push($data_stages, ['id' => '0', 'name' => '结果预览']);

            $data['code'] = 200;
            $data['data']['list'] = $list['data'];
            $data['data']['data_stages'] = $data_stages;
            $data['data']['data_categories'] = $data_categories;
            $data['data']['data_materials'] = $data_materials;
            $data['data']['data_suppliers'] = $data_suppliers;
            $data['data']['total'] = $list['total'];
        }

        return json($data);
    }

    /**
     * add 添加数据管理（排放源）
     * 
	 * @return void
     */
    public function add(Request $request) {

        if (request()->isPost() && $this->validateForm() === true) {
            $data_redis = $request->middleware('data_redis');

            $data['product_calculate_id'] = $_POST['product_calculate_id'];
            $data['product_id'] = $_POST['product_id'];
            $data['data_stage'] = $_POST['data_stage'];
            $data['category'] = $_POST['category'];
            $data['name'] = $_POST['name'];
            $data['distance'] = empty($_POST['distance']) ? NULL : $_POST['distance'];

            // 排放源为运输时
            if (isset($_POST['materials'])) {
                $material_arr = json_decode($_POST['materials'], true);
                $unit_base = $material_arr[0]['unit'];
                $materials = array();
                $numbers = array();
                $units = array();

                $data['number'] = 0;
                $data['unit'] = $unit_base;
                foreach ($material_arr as $key => $value) {
                    $data['number'] += $value['number'];

                    if (array_search($value['name'], $materials)) {
                        $numbers[array_search($value['name'], $materials)] += $value['number'];
                    } else {
                        $materials[$key + 1] = $value['name'];
                        $numbers[$key + 1] = $value['number'];
                        $units[$key + 1] = $data['unit'];
                    }
                }

                if (!is_null($data['distance'])) {
                    $data['number'] = $data['number'] * $data['distance'];
                }

                $data['materials'] = implode(',', $materials);
                $data['numbers'] = implode(',', $numbers);
                $data['units'] = implode(',', $units);
            } else {
                $data['number'] = empty($_POST['number']) ? NULL : $_POST['number'];
                $data['unit'] = empty($_POST['unit']) ? NULL : $_POST['unit'];
                $data['material'] = empty($_POST['material']) ? NULL : $_POST['material'];
            }

            $data['unit_type'] = empty($_POST['unit_type']) ? NULL : $_POST['unit_type'];
            $data['code'] = empty($_POST['code']) ? NULL : $_POST['code'];
            $data['specs'] = empty($_POST['specs']) ? NULL : $_POST['specs'];
            $data['source'] = empty($_POST['source']) ? NULL : $_POST['source'];
            $data['supplier_id'] = empty($_POST['supplier_id']) ? NULL : $_POST['supplier_id'];
            $data['escaping_gas'] = empty($_POST['escaping_gas']) ? NULL : $_POST['escaping_gas'];
            $data['match_type'] = empty($_POST['match_type']) ? 1 : $_POST['match_type'];
            $data['create_by'] = $data_redis['userid'];
            $data['create_time'] = date('Y-m-d H:i:s');
            $data['modify_by'] = $data_redis['userid'];
            $data['modify_time'] = date('Y-m-d H:i:s');

            // 验证当前核算下，当前排放源（名称 + 编号）是否存在
            if ($this->checkProductData($data['name'], $data['code'], $data['product_calculate_id'])) {
                $db = new ProductDataModel();
                $add = $db->addDataManagement($data);
            } else {
                $datasmg['code'] = 201;
                $datasmg['message'] = "排放源已存在，请重新输入";

                return json($datasmg);
            }

            // 添加操作日志
            $data_log['user_id'] = $data['create_by'];
            $data_log['menu_id'] = '8'; // $_POST['menu_id']; // T.B.D 前端未传先注释
            $data_log['time'] = $data['create_time'];
            $data_log['url'] = 'productdata/add'; // $_POST['url']; // T.B.D 前端未传先注释
            $data_log['log'] = '新建产品核算排放源：' . $data['name'];

            $db = new OperationModel();
            $db->addOperation($data_log);

            if ($add) {
                $datasmg['code'] = 200;
                $datasmg['message'] = "添加成功";
            } else {
                $datasmg['code'] = 404;
                $datasmg['message'] = "添加失败";
            }

        } else {
            $datasmg['code'] = 404;
            $datasmg['message'] = $this->validateForm();
        }

        return json($datasmg);
    }

    /**
     * edit 修改数据管理（排放源）
     * 
	 * @return void
     */
    public function edit(Request $request) {

        if (request()->isPost() && $this->validateForm() === true) {
            $data_redis = $request->middleware('data_redis');

            $data['id'] = $_POST['id'];
            $data['product_id'] = $_POST['product_id'];
            $data['product_calculate_id'] = $_POST['product_calculate_id'];
            $data['data_stage'] = $_POST['data_stage'];
            $data['category'] = $_POST['category'];
            $data['name'] = $_POST['name'];
            $data['distance'] = empty($_POST['distance']) ? NULL : $_POST['distance'];

            // 排放源为运输时
            if (isset($_POST['materials'])) {
                $material_arr = json_decode($_POST['materials'], true);
                $unit_base = $material_arr[0]['unit'];
                $materials = array();
                $numbers = array();
                $units = array();

                $data['number'] = 0;
                $data['unit'] = $unit_base;
                foreach ($material_arr as $key => $value) {
                    $data['number'] += $value['number'];

                    if (array_search($value['name'], $materials)) {
                        $numbers[array_search($value['name'], $materials)] += $value['number'];
                    } else {
                        $materials[$key + 1] = $value['name'];
                        $numbers[$key + 1] = $value['number'];
                        $units[$key + 1] = $data['unit'];
                    }
                }
                
                if (!is_null($data['distance'])) {
                    $data['number'] = $data['number'] * $data['distance'];
                }

                $data['materials'] = implode(',', $materials);
                $data['numbers'] = implode(',', $numbers);
                $data['units'] = implode(',', $units);
            } else {
                $data['number'] = empty($_POST['number']) ? NULL : $_POST['number'];
                $data['unit'] = empty($_POST['unit']) ? NULL : $_POST['unit'];
                $data['material'] = empty($_POST['material']) ? NULL : $_POST['material'];
            }

            $data['unit_type'] = empty($_POST['unit_type']) ? NULL : $_POST['unit_type'];
            $data['code'] = empty($_POST['code']) ? NULL : $_POST['code'];
            $data['specs'] = empty($_POST['specs']) ? NULL : $_POST['specs'];
            $data['source'] = empty($_POST['source']) ? NULL : $_POST['source'];
            $data['supplier_id'] = empty($_POST['supplier_id']) ? NULL : $_POST['supplier_id'];
            $data['escaping_gas'] = empty($_POST['escaping_gas']) ? NULL : $_POST['escaping_gas'];
            $data['match_type'] = empty($_POST['match_type']) ? 1 : $_POST['match_type'];
            $data['modify_by'] = $data_redis['userid'];
            $data['modify_time'] = date('Y-m-d H:i:s');

            // 添加操作日志
            $data_log['user_id'] = $data['modify_by'];
            $data_log['menu_id'] = '8';// $_POST['menu_id']; // T.B.D 前端未传先注释
            $data_log['time'] = $data['modify_time'];
            $data_log['url'] = 'productdata/edit';// $_POST['url']; // T.B.D 前端未传先注释
            $data_log['log'] = '编辑产品核算排放源：' . $data['name'];

            $db = new OperationModel();
            $db->addOperation($data_log);

            $db = new ProductDataModel();
            $edit = $db->editDataManagement($data);

            if ($edit) {
                $datasmg['code'] = 200;
                $datasmg['message'] = "修改成功";
            } else {
                $datasmg['code'] = 404;
                $datasmg['message'] = "修改失败";
            }

        } else {
            $datasmg['code'] = 404;
            $datasmg['message'] = $this->validateForm();
        }

        return json($datasmg);
    }

    /**
     * del 删除数据管理（排放源）
     * 
	 * @return void
     */
    public function del(Request $request) {
        $data_redis = $request->middleware('data_redis');

        $id = $_POST['id'];

        // 添加操作日志
        $data['user_id'] = $data_redis['userid'];
        $data_log['menu_id'] = '8';// $_POST['menu_id']; // T.B.D 前端未传先注释
        $data_log['time'] = date('Y-m-d H:i:s');
        $data_log['url'] = 'productdata/del';// $_POST['url']; // T.B.D 前端未传先注释
        $data_log['log'] = '删除数据管理（排放源）';

        $db = new OperationModel();
        $db->addOperation($data_log);

        $db = new ProductDataModel();
        $del = $db->delDataManagement($id);

        if($del){
            $datasmg['code'] = 200;
            $datasmg['message'] = "删除成功";
        }else{
            $datasmg['code'] = 404;
            $datasmg['message'] = "删除失败";
        }

        return json($datasmg);
    }

    /**
     * checkProductData 验证产品数据管理
     * 
     * @param $name
     * @param $code
     * @param $product_calculate_id
	 * @return void
     */
    public function checkProductData($name, $code, $product_calculate_id) {
        $db = new ProductDataModel();
        $list = $db->getAllProductDatas($product_calculate_id)->toArray();

        $combine_products = array();
        foreach ($list as $key => $value) {
            $combine_products[$key] = $value['name'] . '-' . $value['code'];
        }

        $combine_product = $name . '-' . $code;

        if (in_array($combine_product, $combine_products)) {
            return false;
        } else {
            return  true;
        }
    }


    /**
     * choice 选择排放因子/核算产品 T.B.D供应商产品
     * 
	 * @return void
     */
    public function choice(Request $request) {
        $data_redis = $request->middleware('data_redis');
        $data['id'] = $_POST['product_data_id']; // 产品数据管理id
        $data['match_id'] = $_POST['id']; // 匹配因子/产品id
        $data['modify_by'] = $data_redis['userid'];
        $data['modify_time'] = date('Y-m-d H:i:s');

        $db = new ProductDataModel();
        $product_data = $db->getDataManagement($data['id']);
        $data['product_calculate_id'] = $product_data['product_calculate_id']; // 产品核算id
        $calculate_data = $db->getProductCalculatesByMatchType($data['product_calculate_id']);
        $data['number'] = $calculate_data['number']; // 产品核算数量

        // 匹配类型：1-因子，2-产品
        if ($product_data['match_type'] == 1) {
            $data['coefficient'] = $_POST['factor_value'];
            $data['emissions'] = $_POST['factor_value'] * $product_data['number'];
        } elseif ($product_data['match_type'] == 2) {
            $data['coefficient'] = $_POST['coefficient'];
            $data['emissions'] = $_POST['coefficient'] * $product_data['number'];
        }

        // 选择排放因子/核算产品
        $edit = $db->choiceFactor($data);

        if ($edit) {
            $datasmg['code'] = 200;
            $datasmg['message'] = "修改成功";
        } else {
            $datasmg['code'] = 404;
            $datasmg['message'] = "修改失败";
        }

        return json($datasmg);
    }

	//======================================================================
	// PROTECTED FUNCTIONS
	//======================================================================

    /**
     * validateForm 验证
     * 
	 * @return void
     */
    protected function validateForm() {
        $data = request()->param();
        $transport_ids = [8,12,14,16,18];

        try {

            if (in_array((int)$data['category'], $transport_ids)) {
                validate(ProductDataTransportValidate::class)->check($data);
            } else {
                validate(ProductDataValidate::class)->check($data);
            }

            return true;
        } catch (ValidateException $e) {
            
            return $e->getError();
        }
    }
}