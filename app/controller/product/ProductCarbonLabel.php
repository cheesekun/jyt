<?php


namespace app\controller\product;


use app\BaseController;
use app\model\product\ProductCalculateModel;
use app\model\product\ProductCarbonLabelModel;
use app\model\product\ProductModel;

use app\model\system\OperationModel;
use app\validate\ProductCarbonLabelValidate;
use think\exception\ValidateException;
use think\Request;

class ProductCarbonLabel extends BaseController
{
    /**
     * @title 碳中和标签列表
     * @author fengweizhe
     */
    public function index() {

    }

    public function addForm() {
        $db = new ProductModel();
        //查询出产品信息
        $list = $db->getCarconLabelAddFormProduct()->toArray();
        //根据产品id 查询出产品的核算时间
        $product_ids = array_column($list, 'id');
        $db1 = new ProductCalculateModel();
        $product_week_date_info = $db1->getCalculateWeekByProdectIds($product_ids);
        $product_week_arr_data = [];
        foreach ($product_week_date_info as $k => $v) {
            $product_week_arr_data[$v['product_id']] = $v;
        }
        //把核算时间加入到返回的信息中
        $return_list = [];
        foreach ($list as $kk => $vv) {
            if (isset($product_week_arr_data[$vv['id']])) {
                $vv['week_date'] = $product_week_arr_data[$vv['id']];
            } else {
                $vv['week_date'] = [];
            }
            $return_list[$kk] = $vv;
        }
        return json($return_list);
    }

    /**
     * @title 碳中和新增
     * @author fengweizhe
     */
    public function add(Request $request) {
        if (request()->isPost() && $this->validateForm() === true) {
            $data_redis = $request->middleware('data_redis');
            $data['carbon_label_name'] = $_POST['carbon_label_name'];
            $data['product_id'] = $_POST['product_id'];
            $data['week_start'] = $_POST['week_start'];
            $data['week_end'] = $_POST['week_end'];
            $data['introduce'] = isset($_POST['introduce']) ? $_POST['introduce'] : '';
            $data['url'] = isset($_POST['url']) ? $_POST['url'] : '';
            $data['create_by'] = $data_redis['userid'];
            $data['modify_by'] = $data_redis['userid'];
            $data['create_time'] = date('Y-m-d H:i:s');
            $data['modify_time'] = date('Y-m-d H:i:s');
            $data['is_del'] = ProductCarbonLabelModel::IS_DEL_NOT_DEL;

            // 添加操作日志
//            $data_log['user_id'] = $data['create_by'];
//            $data_log['menu_id'] = '8';// $_POST['menu_id']; // T.B.D 前端未传先注释
//            $data_log['time'] = $data['create_time'];
//            $data_log['url'] = 'product/add';// $_POST['url']; // T.B.D 前端未传先注释
//            $data_log['log'] = '添加产品：' . $data['product_name'];

//            $db = new OperationModel();
//            $db->addOperation($data_log);
            $db = new ProductCarbonLabelModel();
            $add = $db->addProductCarbonLabel($data);

            if ($add) {
                $datasmg['code'] = 200;
                $datasmg['message'] = "添加成功";
            } else {
                $datasmg['code'] = 404;
                $datasmg['message'] = "添加失败";
            }

        } else {
            $datasmg['code'] = 404;
            $datasmg['message'] = $this->validateForm();
        }

        return json($datasmg);
    }

    /**
     * @title 碳中和删除
     * @author fengweizhe
     */
    public function del(Request $request) {
//        $data_redis = $request->middleware('data_redis');

//        $data['product_name'] = '示例产品A'; // $_POST['product_name']; // T.B.D 暂不确定->是否为核算名称+核算年份
//
//        // 添加操作日志
//        $data['user_id'] = $data_redis['userid'];
//        $data_log['menu_id'] = '8';// $_POST['menu_id']; // T.B.D 前端未传先注释
//        $data_log['time'] = date('Y-m-d H:i:s');
//        $data_log['url'] = 'productcalculate/del';// $_POST['url']; // T.B.D 前端未传先注释
//        $data_log['log'] = '删除产品核算：' . $data['product_name'];
//
//        $db = new OperationModel();
//        $db->addOperation($data_log);
        $data['id'] = $_POST['id'];
        $data['is_del'] = ProductCarbonLabelModel::IS_DEL_DEL;
        $db = new ProductCarbonLabelModel();
        $del = $db->delProductCarbonLabel($data);

        if($del){
            $datasmg['code'] = 200;
            $datasmg['message'] = "删除成功";
        }else{
            $datasmg['code'] = 404;
            $datasmg['message'] = "删除失败";
        }

        return json($datasmg);
    }

    /**
     * @title 碳中和编辑
     * @author fengweizhe
     */
    public function edit(Request $request) {
        if (request()->isPost() && $this->validateForm() === true) {
            $data_redis = $request->middleware('data_redis');
            $data['id'] = $_POST['id'];
            $data['carbon_label_name'] = $_POST['carbon_label_name'];
            $data['product_id'] = $_POST['product_id'];
            $data['week_start'] = $_POST['week_start'];
            $data['week_end'] = $_POST['week_end'];
            $data['introduce'] = isset($_POST['introduce']) ? $_POST['introduce'] : '';
            $data['url'] = isset($_POST['url']) ? $_POST['url'] : '';
            $data['modify_by'] = $data_redis['userid'];
            $data['modify_time'] = date('Y-m-d H:i:s');

            // 添加操作日志
//            $data_log['user_id'] = $data['modify_by'];
//            $data_log['menu_id'] = '8';// $_POST['menu_id']; // T.B.D 前端未传先注释
//            $data_log['time'] = $data['modify_time'];
//            $data_log['url'] = 'productcalculate/edit';// $_POST['url']; // T.B.D 前端未传先注释
//            $data_log['log'] = '编辑产品核算：' . $_POST['product_name'];
//
//            $db = new OperationModel();
//            $db->addOperation($data_log);

            $db = new ProductCarbonLabelModel();
            $edit = $db->editProductCarbonLabel($data);

            if ($edit) {
                $datasmg['code'] = 200;
                $datasmg['message'] = "修改成功";
            } else {
                $datasmg['code'] = 404;
                $datasmg['message'] = "修改失败";
            }

        } else {
            $datasmg['code'] = 404;
            $datasmg['message'] = $this->validateForm();
        }

        return json($datasmg);
    }

    /**
     * @title 碳中和详情
     * @author fengweizhe
     */
    public function info() {

    }

    /**
     * validateForm 验证
     *
     * @return void
     */
    protected function validateForm() {
        $data = request()->param();

        try {
            validate(ProductCarbonLabelValidate::class)->check($data);

            return  true;
        } catch (ValidateException $e) {

            return $e->getError();
        }
    }
}