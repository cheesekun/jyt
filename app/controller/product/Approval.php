<?php
declare (strict_types = 1);

namespace app\controller\product;

use app\BaseController;
use app\model\product\ApprovalModel;
use app\model\product\ProductModel;
use app\model\product\ProductCalculateModel;
use app\model\system\OperationModel;
use think\Request;

/**
 * Approval
 */
class Approval extends BaseController {

	//======================================================================
	// PUBLIC FUNCTIONS
	//======================================================================

    /**
     * index 数据审批列表
     * 
	 * @return void
     */
    public function index() {
        $page_size = isset($_GET['pageSize']) ? $_GET['pageSize'] : '';
        $page_index = isset($_GET['pageIndex']) ? $_GET['pageIndex'] : '';

        // 过滤条件：产品名称、产品编号、核算周期开始、核算周期结束
        $filters = [
            'filter_product_name' => isset($_GET['filterProductName']) ? $_GET['filterProductName'] : '',
            'filter_product_no' => isset($_GET['filterProductNo']) ? $_GET['filterProductNo'] : '',
            'filter_week_start' => isset($_GET['filterWeekStart']) ? $_GET['filterWeekStart'] : '',
            'filter_week_end' => isset($_GET['filterWeekEnd']) ? $_GET['filterWeekEnd'] : ''
        ];

        $db = new ApprovalModel();
        $list = $db->getApprovals($page_size, $page_index, $filters)->toArray();

        $db = new ProductModel();

        $array = array();
        foreach ($list['data'] as $key => $value) {
            if ($value['files'] != NULL) {
                foreach (explode(',', $value['files']) as $file_key => $file_value) {
                    $array[$file_key] = $db->getFiles($file_value);
                }
            }

            $list['data'][$key]['files'] = $array;
        }

        $data['code'] = 200;
        $data['data']['list'] = $list['data'];
        $data['data']['total'] = $list['total'];

        return json($data);
    }

    /**
     * state 更新数据审批状态
     * 
     * @param $request
	 * @return void
     */
    public function state(Request $request) {

        if (request()->isPost()) {
            $data_redis = $request->middleware('data_redis');
            $data['id'] = $_POST['id'];
            $data['modify_by'] = $data_redis['userid'];
            $data['state'] = $_POST['state'];
            $data['modify_time'] = date('Y-m-d H:i:s');

            // 添加操作日志
            $data_log['user_id'] = $data['modify_by'];
            $data_log['menu_id'] = '8';// $_POST['menu_id']; // T.B.D 前端未传先注释
            $data_log['time'] = $data['modify_time'];
            $data_log['url'] = 'approval/state';// $_POST['url']; // T.B.D 前端未传先注释
            $data_log['log'] = '更新数据审批状态：' . $_POST['product_name'];

            $db = new OperationModel();
            $db->addOperation($data_log);

            $db = new ApprovalModel();
            $edit = $db->updateState($data);

            if ($edit) {
                $datasmg['code'] = 200;
                $datasmg['message'] = "审核通过";
            } else {
                $datasmg['code'] = 404;
                $datasmg['message'] = "审核失败";
            }

        } else {
            $datasmg['code'] = 404;
            $datasmg['message'] = "审核失败";
        }

        return json($datasmg);
    }

    /**
     * see 查看数据审批详情
     * 
	 * @return void
     */
    public function see() {
        $id = $_GET['id'];

        $db = new ProductCalculateModel();
        $list = $db->getCalculate($id);
        $list_stage = explode(',', $list['stage']);
        $list_all = $db->seeCalculateData($id);

        $array = array();
        foreach ($list_stage as $list_key => $list_value) {
            foreach ($list_all as $value) {
                if ($value['data_stage'] == $list_value) {
                    $name = $db->getStageName($list_key + 1);
                    $array[$list_key]['data_stage'] = $name['name'];
                    $array[$list_key]['datas'][] = $value;
                }
            }
        }

        $data['code'] = 200;
        $data['data']['list'] = $array;
        $data['data']['total_emissions'] = $list['emissions'];

        return json($data);
    }
}