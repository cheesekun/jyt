<?php
namespace app\controller\product;

use app\BaseController;
use app\validate\ProductValidate;
use think\exception\ValidateException;
use app\model\product\ProductModel;
use app\model\system\OperationModel;
use think\Request;

/**
 * Product
 */
class Product extends BaseController {

	//======================================================================
	// PUBLIC FUNCTIONS
	//======================================================================

    /**
     * index 产品列表
     * 
	 * @return void
     */
    public function index() {
        $page_size = isset($_GET['pageSize']) ? $_GET['pageSize'] : '';
        $page_index = isset($_GET['pageIndex']) ? $_GET['pageIndex'] : '';

        // 过滤条件：产品名称、产品编号、产品规格、产品状态
        $filters = [
            'filter_product_name' => isset($_GET['filterProductName']) ? $_GET['filterProductName'] : '',
            'filter_product_no' => isset($_GET['filterProductNo']) ? $_GET['filterProductNo'] : '',
            'filter_product_spec' => isset($_GET['filterProductSpec']) ? $_GET['filterProductSpec'] : '',
            'filter_product_state' => isset($_GET['filterState']) ? $_GET['filterState'] : ''
        ];

        $db = new ProductModel();
        $list = $db->getProducts($page_size, $page_index, $filters)->toArray();

        foreach ($list['data'] as $key => $value) {
            $calculate_list = $db->seeProductCalculate($value['id'])->toarray();
            $list['data'][$key]['count'] = count($calculate_list);
        }

        $active_products = $db->getActiveProducts()->toArray();

        $array = array();
        foreach ($list['data'] as $key => $value) {
            if ($value['files'] != NULL) {
                foreach (explode(',', $value['files']) as $file_key => $file_value) {
                    $array[$file_key] = $db->getFiles($file_value);
                }
            }

            $list['data'][$key]['files'] = $array;
        }

        $data['code'] = 200;
        $data['data']['list'] = $list['data'];
        $data['data']['total'] = $list['total'];
        $data['data']['active'] = $active_products;

        return json($data);
    }

    /**
     * add 产品添加
     * 
     * @param $request
	 * @return void
     */
    public function add(Request $request) {

        if (request()->isPost() && $this->validateForm() === true) {
            $data_redis = $request->middleware('data_redis');

            $data['virtual_id'] = empty($_POST['virtual_id']) ? NULL : $_POST['virtual_id']; // 虚拟id
            $data['files'] = isset($_POST['files']) ? json_decode($_POST['files'], true) : NULL; // 产品文件
            $data['product_name'] = $_POST['product_name'];
            $data['product_no'] = $_POST['product_no'];
            $data['product_spec'] = empty($_POST['product_spec']) ? NULL : $_POST['product_spec'];
            $data['state'] = '1';
            $data['create_by'] = $data_redis['userid'];
            $data['modify_by'] = $data_redis['userid'];
            $data['create_time'] = date('Y-m-d H:i:s');
            $data['modify_time'] = date('Y-m-d H:i:s');

            // 验证当前产品（产品名称 + 产品编号）是否存在
            if ($this->checkProduct($data['product_name'], $data['product_no'])) {
                $db = new ProductModel();
                $add = $db->addProduct($data);
            } else {
                $datasmg['code'] = 201;
                $datasmg['message'] = "产品已存在，请重新输入";

                return json($datasmg);
            }

            // 添加操作日志
            $data_log['user_id'] = $data['create_by'];
            $data_log['menu_id'] = '8';// $_POST['menu_id']; // T.B.D 前端未传先注释
            $data_log['time'] = $data['create_time'];
            $data_log['url'] = 'product/add';// $_POST['url']; // T.B.D 前端未传先注释
            $data_log['log'] = '添加产品：' . $data['product_name'];

            $db = new OperationModel();
            $db->addOperation($data_log);

            if ($add) {
                $datasmg['code'] = 200;
                $datasmg['message'] = "添加成功";
            } else {
                $datasmg['code'] = 404;
                $datasmg['message'] = "添加失败";
            }

        } else {
            $datasmg['code'] = 404;
            $datasmg['message'] = $this->validateForm();
        }
        
        return json($datasmg);
    }

    /**
     * edit 产品修改
     * 
     * @param $request
	 * @return void
     */
    public function edit(Request $request) {

        if (request()->isPost() && $this->validateForm() === true) {
            $data_redis = $request->middleware('data_redis');

            $data['id'] = $_POST['id'];
            $data['product_name'] = $_POST['product_name'];
            $data['product_no'] = $_POST['product_no'];
            $data['files'] = isset($_POST['files']) ? json_decode($_POST['files'], true) : NULL; // 产品文件
            $data['product_spec'] = empty($_POST['product_spec']) ? NULL : $_POST['product_spec'];
            $data['modify_by'] = $data_redis['userid'];
            $data['modify_time'] = date('Y-m-d H:i:s');

            // 添加操作日志
            $data_log['user_id'] = $data['modify_by'];
            $data_log['menu_id'] = '8';// $_POST['menu_id']; // T.B.D 前端未传先注释
            $data_log['time'] = $data['modify_time'];
            $data_log['url'] = 'product/edit';// $_POST['url']; // T.B.D 前端未传先注释
            $data_log['log'] = '编辑产品：' . $data['product_name'];

            $db = new OperationModel();
            $db->addOperation($data_log);

            $db = new ProductModel();
            $edit = $db->editProduct($data);

            if ($edit) {
                $datasmg['code'] = 200;
                $datasmg['message'] = "修改成功";
            } else {
                $datasmg['code'] = 404;
                $datasmg['message'] = "修改失败";
            }

        } else {
            $datasmg['code'] = 404;
            $datasmg['message'] = $this->validateForm();
        }

        return json($datasmg);
    }

    /**
     * state 更新产品状态
     * 
     * @param $request
	 * @return void
     */
    public function state(Request $request) {

        if (request()->isPost()) {
            $data_redis = $request->middleware('data_redis');
            $data['id'] = $_POST['id'];
            $data['modify_by'] = $data_redis['userid'];
            $data['state'] = $_POST['state'];
            $data['modify_time'] = date('Y-m-d H:i:s');

            // 添加操作日志
            $data_log['user_id'] = $data['modify_by'];
            $data_log['menu_id'] = '8';// $_POST['menu_id']; // T.B.D 前端未传先注释
            $data_log['time'] = $data['modify_time'];
            $data_log['url'] = 'product/state';// $_POST['url']; // T.B.D 前端未传先注释
            $data_log['log'] = '更新产品状态';

            $db = new OperationModel();
            $db->addOperation($data_log);

            $db = new ProductModel();
            $edit = $db->updateState($data);

            if ($edit) {
                $datasmg['code'] = 200;
                $datasmg['message'] = "修改成功";
            } else {
                $datasmg['code'] = 404;
                $datasmg['message'] = "修改失败";
            }

        } else {
            $datasmg['code'] = 404;
            $datasmg['message'] = "修改失败";
        }

        return json($datasmg);
    }

    /**
     * see 查看产品详情
     * 
	 * @return void
     */
    public function see() {
        $id = $_GET['id'];

        $db = new ProductModel();
        $list = $db->seeProduct($id)->toarray();
        $calculate_list = $db->seeProductCalculate($id);

        $data['code'] = 200;
        $data['data']['list'] = $list;
        $data['data']['calculate_list'] = $calculate_list;

        return json($data);
    }

    /**
     * checkProduct 验证产品
     * 
     * @param $product_name
     * @param $product_no
	 * @return void
     */
    public function checkProduct($product_name, $product_no) {
        $db = new ProductModel();
        $list = $db->getAllProducts()->toArray();

        $combine_products = array();
        foreach ($list as $key => $value) {
            $combine_products[$key] = $value['product_name'] . '-' . $value['product_no'];
        }

        $combine_product = $product_name . '-' . $product_no;

        if (in_array($combine_product, $combine_products)) {
            return false;
        } else {
            return  true;
        }
    }

    /**
     * upload 上传文件
     * 
     * @param $request
	 * @return void
     */
    public function upload(Request $request){
        $data_redis = $request->middleware('data_redis');
        $file = $request->file();

        try{
            // 验证文件大小格式
            validate(['file'=>'fileSize:1048576|fileExt:png,jpg,doc,docx,pdf,xls,xlsx'])
            ->check($file);
 
            $savename = \think\facade\Filesystem::disk('public')->putFile('uploads', $file['file']);
            $url = \think\facade\Filesystem::getDiskConfig('public', 'url') . '/' . str_replace('\\', '/', $savename);
            $data['virtual_id'] = isset($_POST['virtual_id']) ? $_POST['virtual_id'] : NULL;
            $data['file_path'] = $url;
            $data['source_name'] = $_FILES['file']['name'];
            $data['file_size'] = $_FILES['file']['size'];
            $data['create_by'] = $data_redis['userid'];
            $data['modify_by'] = $data_redis['userid'];
            $data['create_time'] = date('Y-m-d H:i:s');
            $data['modify_time'] = date('Y-m-d H:i:s');

            $db = new ProductModel();
            $add_id = $db->addProductFile($data);

            if ($add_id) {
                $datasmg['code'] = 200;
                $datasmg['id'] = (int)$add_id;
                $datasmg['name'] = $data['source_name'];
                $datasmg['msg'] = "上传成功"; // 前端要求， mseeage -> msg，不提示弹框
            } else {
                $datasmg['code'] = 404; // 前端要求，上传失败 404 -> 200，不提示弹框
                $datasmg['name'] = $data['source_name']; // 前端要求，上传失败返回文件名
                $datasmg['msg'] = "上传失败";
            }

            return json($datasmg);
        }catch(ValidateException $e){
            $datasmg['code'] = 200; // 前端要求，验证失败 404 -> 200，不提示弹框
            $datasmg['name'] = $_FILES['file']['name']; // 前端要求，验证失败返回文件名
            $datasmg['msg'] = $e->getError();

            return json($datasmg);
        }
    }

    /**
     * download 产品文件下载
     * 
	 * @return void
     */
    public function download() {
        $id = $_GET['id'];

        $db = new ProductModel();
        $list = $db->getFile($id);

        $filePath = '.' . $list['file_path'];
        $fileName = $list['source_name'];

        return download($filePath, $fileName)->expire(300);
    }

	//======================================================================
	// PROTECTED FUNCTIONS
	//======================================================================

    /**
     * validateForm 验证
     * 
	 * @return void
     */
    protected function validateForm() {
        $data = request()->param();

        try {
            validate(ProductValidate::class)->check($data);

            return  true;
        } catch (ValidateException $e) {
            
            return $e->getError();
        }
    }
}