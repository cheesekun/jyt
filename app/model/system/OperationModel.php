<?php
namespace app\model\system;

use think\facade\Db;

/**
 * OperationModel
 */
class OperationModel extends Db {

	//======================================================================
	// PUBLIC FUNCTIONS
	//======================================================================

    /**
     * getOperations 查询操作日志
     * 
     * @param $page_ize
     * @param $page_index
     * @param $filters
	 * @return $list
     */
    public function getOperations($page_size, $page_index, $filters) {
        $where = array();

        if ($filters['filter_user_name']) {
            $where[] = array(['ju.username', 'like', '%' . trim($filters['filter_user_name']) . '%']);
        }

        if ($filters['filter_module']) {
            $where[] = array(['jol.menu_id', 'like', '%' . trim($filters['filter_module']) . '%']);
        }

        if ($filters['filter_time_start']) {
            $where[] = array(['jol.time', '>=', $filters['filter_time_start']]);
        }

        if ($filters['filter_time_end']) {
            $where[] = array(['jol.time', '<=', $filters['filter_time_end']]);
        }

        $list = Db::table('jy_operation_log jol')
            ->field('jol.time, jol.log, jol.url, ju.username, jm.title')
            ->leftJoin('jy_user ju', 'jol.user_id = ju.id')
            ->leftJoin('jy_menu jm', 'jol.menu_id = jm.id')
            ->where($where)
            ->order('jol.id', 'desc')
            ->paginate(['list_rows'=>$page_size, 'page'=>$page_index]);

        return $list;
    }

    /**
     * addOperation 添加操作日志
     * 
     * @param $data
	 * @return $list
     */
    public function addOperation($data) {
        $add = Db::table('jy_operation_log')->insert($data);

        return $add;
    }
 
}