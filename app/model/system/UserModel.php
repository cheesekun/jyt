<?php
namespace app\model\system;

use think\facade\Db;

/**
 * UserModel
 */
class UserModel extends Db {

	//======================================================================
	// PUBLIC FUNCTIONS
	//======================================================================

    /**
     * getUsers 查询用户
     * 
     * @param $page_size
     * @param $page_index
     * @param $filters
	 * @return $list
     */
    public function getUsers($page_size, $page_index, $filters) {
        $where = array();

        if ($filters['filter_user_name']) {
            $where[] = array(['ju.username', 'like', '%' . trim($filters['filter_user_name']) . '%']);
        }

        if ($filters['filter_telephtone']) {
            $where[] = array(['ju.telephone', 'like', '%' . trim($filters['filter_telephtone']) . '%']);
        }

        if ($filters['filter_user_state']) {
            $where[] = array(['ju.state', '=' , trim($filters['filter_user_state'])]);
        }

        $list = Db::table('jy_user ju')
            ->field('ju.id, ju.username, ju.telephone, ju.state, jo.name organization, ju.role_id')
            ->leftJoin('jy_organization jo', 'ju.organization_id = jo.id')
            ->where($where)
            ->order('ju.id', 'desc')
            ->paginate(['list_rows'=>$page_size, 'page'=>$page_index]);

        return $list;
    }

    /**
     * seeUser 查看用户详情
     * 
     * @param $id
	 * @return $list
     */
    public function seeUser($id) {
        $list = Db::table('jy_user ju')
            ->field('ju.id, ju.username, ju.telephone, ju.state, jo.name organization, ju.role_id')
            ->leftJoin('jy_organization jo', 'ju.organization_id = jo.id')
            ->where('ju.id', (int)$id)
            ->find();

        return $list;
    }

    /**
     * addUser 添加用户
     * 
     * @param $data
	 * @return $list
     */
    public function addUser($data) {
        $add = Db::table('jy_user')->insert($data);

        return $add;
    }

    /**
     * updateUser 更新用户
     * 
     * @param $data
	 * @return $edit
     */
    public function updateUser($data) {
        $edit = Db::table('jy_user')->where('id', (int)$data['id'])->update($data);

        return $edit;
    }

     /**
     * getUserByTelephone 通过手机号查找用户
     * 
     * @param $data
	 * @return $list
     */
    public function getUserByTelephone($data) {
        $list = Db::table('jy_user')->where('telephone', $data)->select();

        return $list;
    }

     /**
     * getUserByName 通过用户名查找用户
     * 
     * @param $data
	 * @return $list
     */
    public function getUserByName($data) {
        $list = Db::table('jy_user')->where('username', $data)->select();

        return $list;
    }

     /**
     * getRole 获取角色
     * 
     * @param $id
	 * @return $list
     */
    public function getRole($id) {
        $list = Db::table('jy_role')->where('id', (int)$id)->find();

        return $list;
    }  

     /**
     * getRoles 获取角色列表
     * 
	 * @return $list
     */
    public function getRoles() {
        $list = Db::table('jy_role')->select();

        return $list;
    } 
}