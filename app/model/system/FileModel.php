<?php
namespace app\model\system;

use think\facade\Db;

/**
 * FileModel
 */
class FileModel extends Db {

	//======================================================================
	// PUBLIC FUNCTIONS
	//======================================================================

    /**
     * getFiles 查询下载文件
     * 
     * @param $page_ize
     * @param $page_index
     * @param $filters
	 * @return $list
     */
    public function getFiles($page_size, $page_index, $filters) {
        $where = array();

        if ($filters['filter_user_name']) {
            $where[] = array(['ju.username', 'like', '%' . trim($filters['filter_user_name']) . '%']);
        }

        if ($filters['filter_module']) {
            $where[] = array(['jf.module', 'like', '%' . trim($filters['filter_module']) . '%']);
        }

        if ($filters['filter_time_start']) {
            $where[] = array(['jf.modify_time', '>=', $filters['filter_time_start']]);
        }

        if ($filters['filter_time_end']) {
            $where[] = array(['jf.modify_time', '<=', $filters['filter_time_end']]);
        }

        $where[] = array(['jf.module', 'in', $filters['modules']]);

        $list = Db::table('jy_file jf')
            ->field('jf.id, jf.module, jf.source_name file_name, jf.modify_time, ju.username, jo.name organization, "已完成" state')
            ->leftJoin('jy_user ju', 'jf.create_by = ju.id')
            ->leftJoin('jy_organization jo', 'ju.organization_id = jo.id')
            ->where($where)
            ->order(['jf.modify_time'=>'desc', 'jf.create_time'=>'desc'])
            ->paginate(['list_rows'=>$page_size, 'page'=>$page_index]);

        return $list;
    }

    /**
     * getFile 查询文件
     * 
     * @param $id
	 * @return $list
     */
    public function getFile($id) {
        $list = Db::table('jy_file jf')
            ->field('jf.id, jf.source_name, jf.file_path')
            ->where('id', (int)$id)
            ->find();

        return $list;
    }

    /**
     * addProductFile 添加文件
     * 
     * @param $data
	 * @return $id
     */
    public function addProductFile($data) {
        $add = Db::table('jy_file')->insertGetId($data);

        return $add;
    }

    /**
     * getModules 查询操作模块
     * 
	 * @return $list
     */
    public function getModules() {
        $list = Db::table('jy_menu jm')
            ->field('jm.id, jm.title')
            ->where('pid', 0)
            ->select();

        return $list;
    }
}