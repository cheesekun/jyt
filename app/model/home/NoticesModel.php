<?php
declare (strict_types = 1);

namespace app\model\home;

use think\facade\Db;


/**
 * NoticesModel
 */
class NoticesModel extends Db {
	//======================================================================
	// PUBLIC FUNCTIONS
	//======================================================================
    /**
     * 消息类型-通知公告
     */
    CONST TYPE_NOTICE = 1;
    /**
     * 消息类型-最新咨询
     */
    CONST TYPE_NEWS = 2;

    CONST pageSize = 10; //TODO

    CONST pageIndex = 0; //TODO

    CONST TYPE_MAP = [
        self::TYPE_NOTICE => '通知公告',
        self::TYPE_NEWS => '最新咨询',
    ];

    CONST TYPE_SELECT_MAP = [
        ['id'=>self::TYPE_NOTICE, 'name'=>'通知公告'],
        ['id'=>self::TYPE_NEWS, 'name'=>'最新咨询']
    ];
    /**
     * 是否删除-未删除
     */
    CONST IS_DEL_NOT_DEL = 0;
    /**
     * 是否删除-已删除
     */
    CONST IS_DEL_DEL = 1;

    /**
     * getNotices 获取通知列表
     * 
	 * @return $list
     */
    public function getNotices($page_size, $page_index) {
        $list = Db::table('jy_notices')
            ->field('id, author, title, file_url, state, create_time')
            ->where('is_del', '=', self::IS_DEL_NOT_DEL)
            ->where('type', '=', '1')
            ->where('is_del', '=', self::IS_DEL_NOT_DEL)
            ->order('create_time', 'desc')
            ->paginate(['list_rows' => $page_size, 'page' => $page_index]);

        return $list;
    }

    /**
     * seeNotice 获取通知详情
     * 
     * @param $id
	 * @return $list
     */
    public function seeNotice($id) {
        $list = Db::table('jy_notices')
            ->field('id, title, content, file_url, state, create_time')
            ->where('id', (int)$id)
            ->order('create_time', 'desc')
            ->find();
        return $list;
    }

    /**
     * add 消息发布
     * @param $data
     * @return int|string
     */
    public static function add($data) {
        $add = Db::table('jy_notices')->insert($data);

        return $add;
    }

    /**
     * edit 编辑消息
     *
     * @param $data
     * @return $edit
     */
    public static function edit($data) {
        $edit = Db::table('jy_notices')->where('id', (int)$data['id'])->update($data);

        return $edit;
    }

    /**
     * del 删除消息
     *
     * @param $data
     * @return $del
     */
    public static function del($data) {
        $del = Db::table('jy_notices')->where('id', (int)$data['id'])->update($data);

        return $del;
    }

    /**
     * getList 获取通知列表
     *
     * @return $list
     */
    public static function getList($page_size, $page_index, $filters) {

        $where = array();

        if ($filters['title']) {
            $where[] = array(['title', 'like', '%' . trim($filters['title']) . '%']);
        }
        if (isset($filters['type']) &&  $filters['type'] != '') {
            $where[] = array(['type', '=', $filters['type']]);
        }

        $list = Db::table('jy_notices')
            ->field('id, title, file_url, state, type, create_time, author')
            ->where('is_del', '=', self::IS_DEL_NOT_DEL)
            ->where($where)
            ->order('create_time', 'desc')
            ->paginate(['list_rows' => $page_size, 'page' => $page_index]);

        return $list;
    }

    /**
     * getDataById 获取信息（通过id来获取）
     * @param $id
     * @return $list
     */
    public static function getDataById($id) {
        $list = Db::table('jy_notices jn')
            -> where('jn.id', (int)$id)
            -> find();

        return $list;
    }
}
