<?php


namespace app\model\admin;


use think\facade\Db;

class IndustryModel extends Db
{
    public static function getList() {
        $list = Db::table('jy_industry')
            ->select()
            ->toArray();

        return $list;
    }
}