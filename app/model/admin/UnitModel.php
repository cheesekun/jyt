<?php
namespace app\model\admin;

use think\facade\Db;

/**
 * UnitModel
 */
class UnitModel extends Db {

	//======================================================================
	// PUBLIC FUNCTIONS
	//======================================================================

    /**
     * getUnits 查询单位
     * 
     * @param $page_ize
     * @param $page_index
     * @param $filters
	 * @return $list
     */
    public function getUnits($page_size, $page_index, $filters) {
        $where = array();

        if ($filters['filter_unit_type']) {
            $where[] = array(['jut.unit_name', 'like', '%' . trim($filters['filter_unit_type']) . '%']);
        }

        if ($filters['filter_unit_name']) {
            $where[] = array(['jyu.name', 'like', '%' . trim($filters['filter_unit_name']) . '%']);
        }

        $list = Db::table('jy_unit jyu')
            ->field('jyu.id, jyu.name, jyu.is_base, 0 + CAST(jyu.conversion_ratio AS CHAR) conversion_ratio, jyu.modify_time, ju.username, jut.unit_name type')
            ->leftJoin('jy_unit_type jut', 'jyu.type_id = jut.id')
            ->leftJoin('jy_user ju', 'jyu.create_by = ju.id')
            ->where($where)
            ->order(['jyu.type_id'=>'asc', 'jyu.id'=>'asc'])
            ->paginate(['list_rows'=>$page_size, 'page'=>$page_index]);

        return $list;
    }

    /**
     * getUnit 查询单位
     * 
     * @param $type
	 * @return $list
     */
    public function getUnit($type) {
        $where = array();

        if (!empty($type)) {
            $where[] = array(['jyu.type', '=', $type]);
        }

        $list = Db::table('jy_unit jyu')
            ->field('jyu.id, jyu.name, 0 + CAST(jyu.conversion_ratio AS CHAR) conversion_ratio, jyu.type_id, jut.unit_name type')
            ->leftJoin('jy_unit_type jut', 'jyu.type_id = jut.id')
            ->where($where)
            ->order(['jyu.type_id'=>'asc', 'jyu.id'=>'asc'])
            ->select();

        return $list;
    }

    /**
     * @notes 根据单位名称查询单位信息
     *
     * @param $name
     * @return array|mixed|Db|\think\Model|null
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getInfoByName($name) {
        $info = Db::table('jy_unit ju')
            ->where('name', $name)
            ->find();

        return $info;
    }

    /**
     * getUnitType 获取单位类型
     * 
     * @param $type
	 * @return $list
     */
    public function getUnitType() {

        $list = Db::table('jy_unit_type jut')
            ->field('jut.id, jut.unit_name')
            ->order('jut.sort', 'asc')
            ->select();

        return $list;
    }

    /**
     * addUnit 添加单位
     * 
     * @param $data
	 * @return $add
     */
    public function addUnit($data) {
        $add = Db::table('jy_unit')->insert($data);

        return $add;
    }

    /**
     * editUnit 编辑单位
     * 
     * @param $data
	 * @return $edit
     */
    public function editUnit($data) {
        $edit = Db::table('jy_unit')->where('id', (int)$data['id'])->update($data);

        return $edit;
    }

    /**
     * delUnit 删除单位
     * 
     * @param $id
	 * @return $del
     */
    public function delUnit($id) {
        $del = Db::table('jy_unit')->where('id', (int)$id)->delete();

        return $del;
    }

    /**
     * findUnit 查看单位详情
     * 
     * @param $id
	 * @return $list
     */
    public function findUnit($id) {
        $list = Db::table('jy_unit jyu')
            ->field('jyu.id, jyu.name, 0 + CAST(jyu.conversion_ratio AS CHAR) conversion_ratio, jyu.type_id, jut.unit_name type')
            ->leftJoin('jy_unit_type jut', 'jyu.type_id = jut.id')
            ->where('jyu.id', (int)$id)
            ->find();

        $base_unit = Db::table('jy_unit jyu')
        ->field('jyu.name')
        ->where(['jyu.is_base'=> 1, 'jyu.type_id' => $list['type_id']])
        ->find();

        $list['base_unit'] = empty($base_unit) ? '' : $base_unit['name'];

        return $list;
    }
}
