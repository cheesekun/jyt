<?php
namespace app\model\product;

use think\facade\Db;

/**
 * ProductMaterialModel
 */
class ProductMaterialModel extends Db {

	//======================================================================
	// PUBLIC FUNCTIONS
	//======================================================================

    /**
     * getMaterials 查询原材料
     * 
     * @param $page_ize
     * @param $page_index
	 * @return $list
     */
    public function getMaterials($product_id, $virtual_id, $page_size, $page_index) {
        $list = Db::table('jy_product_material jpm')
            ->field('jpm.id, jpm.product_material_name, jpm.product_material_no, jpm.product_material_spec, 0 + CAST(jpm.number AS CHAR) count, jpm.unit_type, jpm.unit, ju.name unit_str, 0 + CAST(jpm.weight AS CHAR) weight, jpm.source, jpm.material, jpm.supplier_id supplier_id, js.title')
            ->leftJoin('jy_supplier js', 'js.id = jpm.supplier_id')
            ->leftJoin('jy_unit ju', 'ju.id = jpm.unit')
            ->where('product_id', (int)$product_id)
            ->where('virtual_id', $virtual_id)
            ->order(['jpm.modify_time'=>'desc', 'jpm.create_time'=>'desc'])
            ->paginate(['list_rows'=>$page_size, 'page'=>$page_index]);

        return $list;
    }

    /**
     * addMaterial 添加原材料
     * 
     * @param $data
	 * @return $add
     */
    public function addMaterial($data) {
        $add = Db::table('jy_product_material')->insert($data);

        return $add;
    }

    /**
     * editMaterial 编辑原材料
     * 
     * @param $data
	 * @return $edit
     */
    public function editMaterial($data) {
        $edit = Db::table('jy_product_material')->where('id', (int)$data['id'])->update($data);

        return $edit;
    }

    /**
     * delMaterial 删除原材料
     * 
     * @param $data
	 * @return $del
     */
    public function delMaterial($data) {
        $where = array();

        if (isset($data['product_id'])) {
            $where[] = array(['product_id', '=', (int)$data['product_id']]);
        }

        $del = Db::table('jy_product_material')->where('id', (int)$data['id'])->where($where)->delete();

        return $del;
    }

    /**
     * getSuppliers 获取供应商
     * 
	 * @return $list
     */
    public function getSuppliers() {
        $list = Db::table('jy_supplier js')->field('js.id, js.title supplier_name')->where('state', 1)->select();

        return $list;
    }

    /**
     * getAllProductMaterials 查询所有原材料
     * 
	 * @return $list
     */
    public function getAllProductMaterials() {
        $list = Db::table('jy_product_material jpm')->select();

        return $list;
    }
}