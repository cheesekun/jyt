<?php


namespace app\model\product;




use think\facade\Db;

class ProductCarbonLabelModel extends Db
{
    /**
     * 是否删除-未删除
     */
    CONST IS_DEL_NOT_DEL = 0;
    /**
     * 是否删除-已删除
     */
    CONST IS_DEL_DEL = 1;

    CONST pageSize = 10; //TODO

    CONST pageIndex = 0; //TODO

    /**
     * getCalculates 查询产品核算列表
     *
     * @param $page_ize
     * @param $page_index
     * @param $filters
     * @return $list
     */
    public static function getCalculates($page_size, $page_index, $filters) {
        $where = array();

        if ($filters['filter_product_name']) {
            $where[] = array(['jp.product_name', 'like', '%' . trim($filters['filter_product_name']) . '%']);
        }

        $list = Db::table('jy_product_carbon_label jpcl')
            ->field('ju.name unit_str, jpcl.id, jpcl.product_id, jpcl.carbon_label_name, jpcl.week_start, jpcl.week_end, jpcl.introduce')
            ->field('jp.product_name, jp.product_no, jp.product_spec')
            ->field('jpc.stage')
            ->leftJoin('jy_product jp', 'jp.id = jpcl.product_id')
            ->leftJoin('jy_product_calculate jpc', 'jpc.id = jpcl.product_calculate_id')
            ->leftJoin('jy_unit ju', 'ju.id = jpc.unit')
            ->where($where)
            ->where('jpcl.is_del', (int)self::IS_DEL_NOT_DEL)
            ->order('jpcl.id', 'desc')
            ->paginate(['list_rows' => $page_size, 'page' => $page_index]);

        return $list;
    }

    /**
     * addProductCarbonLabel 添加产品碳标签
     *
     * @param $data
     * @return $add
     */
    public static function addProductCarbonLabel($data) {
        $add = Db::table('jy_product_carbon_label')->insert($data);

        return $add;
    }

    /**
     * editProductCarbonLabel 编辑产品碳标签
     *
     * @param $data
     * @return $edit
     */
    public static function editProductCarbonLabel($data) {
        $edit = Db::table('jy_product_carbon_label')->where('id', (int)$data['id'])->update($data);

        return $edit;
    }

    /**
     * delCalculate 删除产品标签
     *
     * @param $data
     * @return $del
     */
    public static function delProductCarbonLabel($data) {
        $del = Db::table('jy_product_carbon_label')->where('id', (int)$data['id'])->update($data);

        return $del;
    }

    /**
     * getDataById 获取碳标签的信息（通过核算id来获取）
     *
     * @param $id
     * @return $list
     */
    public static function getDataById($id) {
        $list = Db::table('jy_product_carbon_label jpcl')
            -> where('jpcl.id', (int)$id)
            -> find();

        return $list;
    }
}