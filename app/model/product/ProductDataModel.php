<?php
namespace app\model\product;

use think\facade\Db;

/**
 * ProductDataModel
 */
class ProductDataModel extends Db {

	//======================================================================
	// PUBLIC FUNCTIONS
	//======================================================================

    /**
     * getDataManagements 产品核算数据管理列表
     * 
     * @param $product_id
     * @param $page_size
     * @param $page_index
	 * @return $list
     */
    public function getDataManagements($id, $product_id, $data_stage, $page_size, $page_index) {
        $list = Db::table('jy_product_data jpd')
            ->field('jpd.id, jpd.product_calculate_id, jpd.data_stage, jpd.category, jpd.name, jpd.code, jpd.specs, jpd.material, jpd.materials, 0 + CAST(jpd.number AS CHAR) number, jpd.numbers, jpd.unit, ju.name unit_str, jpd.unit_type, jpd.units, jpd.source, jpd.supplier_id, jpd.distance, jpd.match_type, jpd.match_id, 0 + CAST(jpd.emissions AS CHAR) emissions, 0 + CAST(jpd.coefficient AS CHAR) coefficient')
            ->field('js.title')
            ->leftJoin('jy_supplier js', 'js.id = jpd.supplier_id')
            ->leftJoin('jy_unit ju', 'ju.id = jpd.unit')
            ->where('jpd.product_calculate_id', (int)$id)
            ->where('jpd.product_id', (int)$product_id)
            ->where('jpd.data_stage', (int)$data_stage)
            ->where('jpd.is_del', 1)
            ->order(['jpd.modify_time'=>'desc', 'jpd.create_time'=>'desc'])
            ->paginate(['list_rows' => $page_size, 'page' => $page_index]);

        return $list;
    }

    /**
     * getDataManagement 获取产品核算数据管理
     * 
     * @param $id
	 * @return $list
     */
    public function getDataManagement($id) {
        $list = Db::table('jy_product_data jpd')->where('jpd.id', (int)$id)->find();

        return $list;
    }

    /**
     * getDataManagementByStage 获取阶段下的产品核算数据管理
     * 
     * @param $data_stage
	 * @return $list
     */
    public function getDataManagementByStage($data_stage, $product_calculate_id) {
        $list = Db::table('jy_product_data jpd')->field('DISTINCT(jpd.name) material')->where(['jpd.data_stage'=>(int)$data_stage, 'product_calculate_id'=>(int)$product_calculate_id])->select();

        return $list;
    }

    /**
     * addDataManagement 添加产品数据管理
     * 
     * @param $data
	 * @return $add
     */
    public function addDataManagement($data) {

        Db::startTrans();
        try {
            // 新增产品数据管理
            Db::table('jy_product_data')->insert($data);

            // 从产品数据管理表获取产品核算id下排放量合计（kgCO2e）
            $total = Db::table('jy_product_data jpd')->field('SUM(jpd.emissions) emissions')
            ->where(['product_calculate_id' => (int)$data['product_calculate_id'], 'is_del' => 1])->find();

            // 产品核算查询
            $product_calculate_data = Db::table('jy_product_calculate jpc')->where('jpc.id', (int)$data['product_calculate_id'])->find();

            // 更新产品核算表中的排放量（kgCO2e）和碳排放系数
            Db::table('jy_product_calculate jpc')
            ->where('jpc.id', (int)$data['product_calculate_id'])
            ->update([
                'coefficient' => $total['emissions'] / $product_calculate_data['number'],
                'emissions'   => $total['emissions'],
                'modify_by'   => $data['modify_by'],
                'modify_time' => $data['modify_time']
            ]);

            Db::commit();

            return true;
        } catch (\Exception $e) {
            Db::rollback();

            return false;
        }
    }

    /**
     * editDataManagement 编辑产品核算数据管理
     * 
     * @param $data
	 * @return $edit
     */
    public function editDataManagement($data) {

        Db::startTrans();
        try {
            // 获取最新的排放因子
            $list = Db::table('jy_product_data jpd')->field('jpd.coefficient')
            ->where('jpd.id', (int)$data['id'])->find();
            $data['coefficient'] = $list['coefficient'];
            $data['emissions'] = $list['coefficient'] * $data['number'];

            // 编辑产品数据管理
            Db::table('jy_product_data')->where('id', (int)$data['id'])->update($data);

            // 从产品数据管理表获取产品核算id下排放量合计（kgCO2e）
            $total = Db::table('jy_product_data jpd')->field('SUM(jpd.emissions) emissions')
            ->where(['product_calculate_id' => (int)$data['product_calculate_id'], 'is_del' => 1])->find();

            // 产品核算查询
            $product_calculate_data = Db::table('jy_product_calculate jpc')->where('jpc.id', (int)$data['product_calculate_id'])->find();

            // 更新产品核算表中的排放量（kgCO2e）和碳排放系数
            Db::table('jy_product_calculate jpc')
            ->where('jpc.id', (int)$data['product_calculate_id'])
            ->update([
                'coefficient' => $total['emissions'] / $product_calculate_data['number'],
                'emissions'   => $total['emissions'],
                'modify_by'   => $data['modify_by'],
                'modify_time' => $data['modify_time']
            ]);

            Db::commit();

            return true;
        } catch (\Exception $e) {
            Db::rollback();

            return false;
        }
    }

    /**
     * delDataManagement 删除产品核算数据管理
     * 
     * @param $id
	 * @return $del
     */
    public function delDataManagement($id) {

        Db::startTrans();
        try {
            // 产品数据管理查询
            $product_data = Db::table('jy_product_data jpd')->where('jpd.id', (int)$id)->find();
            // 产品核算查询
            $product_calculate_data = Db::table('jy_product_calculate jpc')->where('jpc.id', (int)$product_data['product_calculate_id'])->find();

            // 删除产品数据管理
            Db::table('jy_product_data')->where('id', (int)$id)->update(['is_del' => 2]);

            // 从产品数据管理表获取产品核算id下排放量合计（kgCO2e）
            $total = Db::table('jy_product_data jpd')->field('SUM(jpd.emissions) emissions')
            ->where(['product_calculate_id' => (int)$product_data['product_calculate_id'], 'is_del' => 1])->find();

            // 更新产品核算表中的排放量（kgCO2e）和碳排放系数
            Db::table('jy_product_calculate jpc')
            ->where('jpc.id', (int)$product_data['product_calculate_id'])
            ->update([
                'coefficient' => $total['emissions'] / $product_calculate_data['number'],
                'emissions'   => $total['emissions']
            ]);

            Db::commit();

            return true;
        } catch (\Exception $e) {
            Db::rollback();

            return false;
        }
    }

    /**
     * getCategoryName 获取产品核算阶段类型名称
     * 
     * @param $id
	 * @return $list
     */
    public function getCategoryName($id) {
        $list = Db::table('jy_data_stage jds')->field('jds.id, jds.name')->where('jds.pid', (int)$id)->select();

        return $list;
    }

    /**
     * getStageName 获取产品核算阶段名称
     * 
     * @param $id
	 * @return $list
     */
    public function getStageName($id) {
        $list = Db::table('jy_data_stage jds')->field('jds.name')->where('jds.id', (int)$id)->find();

        return $list;
    }


    /**
     * getDataByCalculateId 获取产品信息（通过核算id来获取）
     *
     * @param $id
     * @return $list
     */
    public static function getDataByCalculateId($id) {
        $list = Db::table('jy_product_data jpd')
            -> field('jpd.emissions, jpd.coefficient, jpd.id, jpd.product_id, jpd.product_calculate_id, jpd.data_stage, jpd.category, jpd.name, 
            jpd.material, 0 + CAST(jpd.number AS CHAR) number, jpd.materials, jpd.numbers, jpd.source, jpd.supplier_id, jpd.match_type, ju.name unit_str, jpd.distance')
            ->leftJoin('jy_unit ju', 'ju.id = jpd.unit')
            -> where('jpd.product_calculate_id', (int)$id)
            ->where('jpd.is_del', 1)
            -> select()
            ->toArray();

        return $list;
    }

    /**
     * getAllProductDatas 查询当前核算下的所有排放源
     * 
     * @param $data
	 * @return $list
     */
    public function getAllProductDatas($product_calculate_id) {
        $list = Db::table('jy_product_data jpd')->where(['jpd.product_calculate_id' => (int)$product_calculate_id, 'jpd.is_del' => 1])->select();

        return $list;
    }

    /**
     * choiceFactor 选择排放因子/核算产品
     * 
     * @param $data
	 * @return $edit
     */
    public function choiceFactor($data) {

        Db::startTrans();
        try {
            
            // 选择排放因子/核算产品，更新产品数据管理表
            Db::table('jy_product_data jpd')
            ->where('jpd.id', (int)$data['id'])
            ->update([
                'match_id'    => $data['match_id'],
                'coefficient' => $data['coefficient'],
                'emissions'   => $data['emissions'],
                'modify_by'   => $data['modify_by'],
                'modify_time' => $data['modify_time']
            ]);

            // 从产品数据管理表获取产品核算id下排放量合计（kgCO2e）
            $total = Db::table('jy_product_data jpd')->field('SUM(jpd.emissions) emissions')
            ->where(['product_calculate_id' => (int)$data['product_calculate_id'], 'is_del' => 1])->find();

            // 更新产品核算表中的排放量（kgCO2e）和碳排放系数
            Db::table('jy_product_calculate jpc')
            ->where('jpc.id', (int)$data['product_calculate_id'])
            ->update([
                'coefficient' => $total['emissions'] / $data['number'],
                'emissions'   => $total['emissions'],
                'modify_by'   => $data['modify_by'],
                'modify_time' => $data['modify_time']
            ]);

            Db::commit();

            return true;
        } catch (\Exception $e) {
            Db::rollback();

            return false;
        }
    }

    /**
     * getFactorsByMatchType 查询所有排放源
     * 
     * @param $id
	 * @return $list
     */
    public function getFactorsByMatchType($id) {
        $list = Db::table('jy_factor jf')->where('jf.id', (int)$id)->find();

        return $list;
    }

    /**
     * getProductCalculatesByMatchType 查询所有排放源
     * 
     * @param $id
	 * @return $list
     */
    public function getProductCalculatesByMatchType($id) {
        $list = Db::table('jy_product_calculate jpc')
        ->field('jpc.*, jp.product_name, ju.name unit_str')
        ->leftJoin('jy_product jp', 'jp.id = jpc.product_id')
        ->leftJoin('jy_unit ju', 'ju.id = jpc.unit')
        ->where('jpc.id', (int)$id)->find();

        return $list;
    }

    /**
     * seeProduct
     * 
     * @param $id
	 * @return $list
     */
    public function seeProduct($id) {
        $list = Db::table('jy_product jp')->field('jp.product_name material')->where('jp.id', (int)$id)->find();

        return $list;
    }
}