<?php


namespace app\model\product;


use think\facade\Db;

class DataStageModel extends Db
{
    /**
     * getInfoByIdArrAndPid 通过pid和 id的数组来获取数据管理分类信息
     * @param $id_arr
     * @param $pid
     * @return array|\think\Collection|Db[]
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getInfoByIdArrAndPid($id_arr, $pid) {
        $list = Db::table('jy_data_stage jds')
            ->field('jds.id, jds.name, jds.pid')
            ->where('jds.pid', (int)0)
            ->whereIn('jds.id',$id_arr)
            ->select()
            ->toArray();
        return $list;
    }

    /**
     * getIdAndName 获取id和名称
     * @return array
     */
    public static function getIdAndName() {
        $list = Db::table('jy_data_stage jds')
            ->field('jds.id, jds.name')
            ->select()
            ->toArray();
        return $list;
    }
}