<?php


namespace app\model\supply;


use think\facade\Db;

class FactorTypeModel extends Db
{
    public static function getList() {
        $list = Db::table('jy_factor_type')
            ->field('id, name')
            ->select()
            ->toArray();

        return $list;
    }
}