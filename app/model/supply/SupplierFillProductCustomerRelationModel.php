<?php


namespace app\model\supply;


use think\facade\Db;

class SupplierFillProductCustomerRelationModel
{
    /**
     * getDataBySupplierProductId通过供应产品id查询产品填报的对应客户信息
     *
     * @param $supplier_product_id
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function getDataBySupplierProductId($supplier_product_id) {
        $list = Db::table('jy_supplier_fill_product_customer_relation jsfpcr')
            -> where('jsfpcr.supplier_product_id', (int)$supplier_product_id)
            -> select()
            ->toArray();
        return $list;
    }

    /**
     * addRelation  新增供应端上报时产品和客户的客户关系信息
     * @param $data
     * @return int|string
     */
    public static function addRelation($data) {
        $add = Db::table('jy_supplier_fill_product_customer_relation')->insertGetId($data);

        return $add;
    }

    /**
     * getInfoBySupplierProductIdAndCustomerId  通过供应商产品id 和 客户id 来获取 绑定的客户产品信息数据
     *
     * @param $supplier_product_id
     * @param $customer_id
     * @return array|mixed|Db|\think\Model|null
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function getInfoBySupplierProductIdAndCustomerId($supplier_product_id, $customer_id) {
        $list = Db::table('jy_supplier_fill_product_customer_relation jsfpcr')
            -> where('jsfpcr.supplier_product_id', (int)$supplier_product_id)
            -> where('jsfpcr.customer_id', (int)$customer_id)
            -> find();
        return $list;
    }

    /**
     * getCustomerTitleBySupplyProductId 通过产品id来获取 对应得客户名称信息
     *
     * @param $supplier_product_id
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function getCustomerTitleBySupplyProductId($supplier_product_id) {
        $list = Db::table('jy_supplier_fill_product_customer_relation jsfpcr')
            ->field('jc.title')
            ->leftJoin('jy_customer jc', 'jc.id = jsfpcr.customer_id')
            -> where('jsfpcr.supplier_product_id', (int)$supplier_product_id)
            -> select()
            ->toArray();
        return $list;
    }

    /**
     * getCustomerTitleBySupplyProductIds 通过产品id的数组 查询其对应得客户名称
     *
     * @param $supplier_product_id_arr
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function getCustomerTitleBySupplyProductIds($supplier_product_id_arr) {
        $list = Db::table('jy_supplier_fill_product_customer_relation jsfpcr')
            ->field('jc.title, jsfpcr.supplier_product_id')
            ->leftJoin('jy_customer jc', 'jc.id = jsfpcr.customer_id')
            -> where('jsfpcr.supplier_product_id', 'in', $supplier_product_id_arr)
            -> select()
            ->toArray();
        return $list;
    }
}