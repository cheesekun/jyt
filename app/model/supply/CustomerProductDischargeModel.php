<?php


namespace app\model\supply;


use think\facade\Db;

class CustomerProductDischargeModel extends Db
{
    /**
     * 是否删除-未删除
     */
    CONST IS_DEL_NOT_DEL = 0;
    /**
     * 是否删除-已删除
     */
    CONST IS_DEL_DEL = 1;
    /**
     * getDataByProductId 获取采购端客户产品碳核算信息详情（通过产品id来获取）
     *
     * @param $custom_product_id
     * @return $list
     */
    public static function getDataByProductId($custom_product_id) {
        $list = Db::table('jy_customer_product_discharge jcpd')
            -> where('jcpd.customer_product_id', (int)$custom_product_id)
            -> select()
            ->toArray();

        return $list;
    }

    /**
     * addCustomerProductDischarge  新增我是采购端的供应产品的排放源
     * @param $data
     * @return int|string
     */
    public static function addCustomerProductDischarge($data) {
        $add = Db::table('jy_customer_product_discharge')->insertAll($data);

        return $add;
    }

    /**
     * delProductDischarge 删除产品排放源
     *
     * @param $id
     * @return $del
     */
    public static function delProductDischarge($id) {
        $del = Db::table('jy_customer_product_discharge')->where('customer_product_id', (int)$id)->update(['is_del'=>CustomerProductDischargeModel::IS_DEL_DEL]);

        return $del;
    }

}