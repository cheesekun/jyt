<?php


namespace app\model\supply;


use think\facade\Db;

class SupplierModel extends Db
{
    /**
     * 启用/禁用-启用
     */
    CONST STATE_YES = 1;
    /**
     * 启用/禁用-禁用
     */
    CONST STATE_NO = 2;
    /**
     * 风险状态-正常
     */
    CONST RISK_STATE_YES = 1;
    /**
     * 风险状态-风险
     */
    CONST RISK_STATE_NO = 2;

    CONST pageSize = 10;

    CONST pageIndex = 0;

    CONST STATE_MAP = [
        self::STATE_YES => '启用',
        self::STATE_NO => '禁用',
    ];

    CONST RISK_STATE_MAP = [
        self::RISK_STATE_YES => '正常',
        self::RISK_STATE_NO => '风险',
    ];

    CONST STATE_SELECT_MAP = [
        ['id'=>self::STATE_YES, 'name'=>'启用'],
        ['id'=>self::STATE_NO, 'name'=>'禁用']
    ];

    CONST RISK_STATE_SELECT_MAP = [
        ['id'=>self::RISK_STATE_YES, 'name'=>'正常'],
        ['id'=>self::RISK_STATE_NO, 'name'=>'风险']
    ];



    /**
     * list 查询客户的供应商列表
     *
     * @param $page_ize
     * @param $page_index
     * @param $filters
     * @return $list
     */
    public static function getList($page_size, $page_index, $filters) {
        $where = array();
        if (isset($filters['title']) && $filters['title'] != '') {
            $where[] = array(['js.title', 'like', '%' . trim($filters['title']) . '%']);
        }
        if (isset($filters['state']) &&  $filters['state'] != '') {
            $where[] = array(['js.state', '=', trim($filters['state'])]);
        }
        if (isset($filters['risk_state']) &&  $filters['risk_state'] != '') {
            $where[] = array(['js.risk_state', '=', trim($filters['risk_state'])]);
        }
        if (isset($filters['industry_id']) &&  $filters['industry_id'] != '') {
            $where[] = array(['js.industry_id', '=', trim($filters['industry_id'])]);
        }

        $list = Db::table('jy_supplier_customer_relation jscr')
            ->field('js.id, js.industry_id, js.title, js.number, js.state, js.risk_state, ji.name as industry_name, js.number')
            ->leftJoin('jy_supplier js', 'jscr.relation_id = js.id')
            ->leftJoin('jy_industry ji', 'ji.id = js.industry_id')
            ->where($where)
            ->where('jscr.main_id', (int)$filters['main_id'])
            ->where('jscr.is_customer', (int)SupplierCustomerRelationModel::IS_CUSTOMER_YES)
            ->order('js.id', 'desc')
            ->paginate(['list_rows' => $page_size, 'page' => $page_index]);

        return $list;
    }

    /**
     * @getCustomerProductAddFormData 客户端添加产品获取供应商相关信息
     *
     * @param $main_id
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function getCustomerProductAddFormData($main_id) {
        $list = Db::table('jy_supplier_customer_relation jscr')
            ->field('js.id, js.title')
            ->leftJoin('jy_supplier js', 'jscr.relation_id = js.id')
            ->leftJoin('jy_industry ji', 'ji.id = js.industry_id')
            ->where('jscr.main_id', (int)$main_id)
            ->where('jscr.is_customer', (int)SupplierCustomerRelationModel::IS_CUSTOMER_YES)
            ->order('js.id', 'desc')
            ->select()
            ->toArray();

        return $list;
    }

    /**
     * getDataById 获取供应商的信息（通过id来获取）
     * @param $id
     * @return $list
     */
    public static function getDataById($id) {
        $list = Db::table('jy_supplier js')
            ->field('js.id, js.industry_id, js.title, js.number, js.state, js.risk_state, 
            ji.name as industry_name, js.user, js.user_name, js.product_count, js.phone, js.email, 
            js.create_by, js.modify_by, js.create_time, js.modify_time')
            ->leftJoin('jy_industry ji', 'ji.id = js.industry_id')
            -> where('js.id', (int)$id)
            -> find();

        return $list;
    }

    /**
     * getTitleById 更具供应商的id查询供应商名称
     *
     * @param $id
     * @return array|mixed|Db|\think\Model|null
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function getTitleById($id) {
        $info = Db::table('jy_supplier js')
            ->field('js.id, js.title')
            -> where('js.id', (int)$id)
            -> find();

        return $info;
    }

    /**
     * getDataByTitle 获取供应商的信息（通过供应商名称）
     * @param $title
     * @return array|mixed|Db|\think\Model|null
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function getDataByTitle($title) {
        $list = Db::table('jy_supplier js')
            -> where('js.title', $title)
            -> find();

        return $list;
    }

    /**
     * updateState 供应商启用/禁用
     * @param $id
     * @param $state
     * @return int
     * @throws \think\db\exception\DbException
     */
    public static function updateState($id, $state) {
        $update_state = Db::table('jy_supplier')->where('id', (int)$id)->update(['state' => $state]);

        return $update_state;
    }

    /**
     * updateRiskState 供应商 风险等级变更
     * @param $id
     * @param $risk_state
     * @return int
     * @throws \think\db\exception\DbException
     */
    public static function updateRiskState($id, $risk_state) {
        $update_state = Db::table('jy_supplier')->where('id', (int)$id)->update(['risk_state' => $risk_state]);

        return $update_state;
    }

    /**
     * delSupplierProduct 删除产品
     *
     * @param $data
     * @return $del
     */
    public static function delSupplierProduct($data) {
        $del = Db::table('jy_supplier_product')->where('id', (int)$data['id'])->update($data);

        return $del;
    }
}