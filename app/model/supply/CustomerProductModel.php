<?php


namespace app\model\supply;


use think\facade\Db;

class CustomerProductModel extends Db
{
    /**
     * 是否第三方认证-有
     */
    CONST AUTH_HAVE = 1;

    /**
     * 石否第三方认证-无
     */
    CONST AUTH_NOT_HAVE = 2;

    /**
     * 启用/禁用-启用
     */
    CONST STATE_YES = 1;
    /**
     * 启用/禁用-禁用
     */
    CONST STATE_NO = 2;
    /**
     * 是否删除-未删除
     */
    CONST IS_DEL_NOT_DEL = 0;
    /**
     * 是否删除-已删除
     */
    CONST IS_DEL_DEL = 1;

    /**
     * 审核状态-编辑中
     */
    CONST AUDIT_STATE_EDITING = 1;

    /**
     * 审核状态-未填报
     */
    CONST AUDIT_STATE_NOT_FILL = 2;

    /**
     * 审核状态-待审核
     */
    CONST AUDIT_STATE_NOT_VERIFY = 3;

    /**
     * 审核状态-已审核
     */
    CONST AUDIT_STATE_VERIFY = 4;

    /**
     * 审核状态-禁用
     */
    CONST AUDIT_STATE_NOT_USE = 5;

    /**
     * 是否有第三方认证map
     */
    CONST AUTH_MAP = [
        self::AUTH_HAVE => '有',
        self::AUTH_NOT_HAVE => '无'
    ];

    /**
     * 是否第三方认证-下拉选择map
     */
    CONST AUTH_SELECT_MAP = [
        ['id'=>self::AUTH_HAVE, 'name'=>'有'],
        ['id'=>self::AUTH_NOT_HAVE, 'name'=>'无'],
    ];

    /**
     * 填报状态map
     */
    CONST AUDIT_STATE_MAP = [
        self::AUDIT_STATE_EDITING => '编辑中',
        self::AUDIT_STATE_NOT_FILL => '未填报',
        self::AUDIT_STATE_NOT_VERIFY => '待审核',
        self::AUDIT_STATE_VERIFY => '已审核',
        self::AUDIT_STATE_NOT_USE => '禁用'
    ];

    /**
     * 填报状态-下拉选择map
     */
    CONST AUDIT_STATE_SELECT_MAP = [
        ['id'=>self::AUDIT_STATE_EDITING, 'name'=>'编辑中'],
        ['id'=>self::AUDIT_STATE_NOT_FILL, 'name'=>'未填报'],
        ['id'=>self::AUDIT_STATE_NOT_VERIFY, 'name'=>'待审核'],
        ['id'=>self::AUDIT_STATE_VERIFY, 'name'=>'已审核'],
        ['id'=>self::AUDIT_STATE_NOT_USE, 'name'=>'禁用']
    ];



    CONST pageSize = 10; //TODO

    CONST pageIndex = 0; //TODO

    public static function getList($page_size, $page_index, $filters) {
        $where = array();
        if (isset($filters['product_name']) && $filters['product_name'] != '') {
            $where[] = array(['jcp.product_name', 'like', '%' . trim($filters['product_name']) . '%']);
        }
        if (isset($filters['product_type']) &&  $filters['product_type'] != '') {
            $where[] = array(['jcp.product_type', '=', trim($filters['product_type'])]);
        }
        if (isset($filters['audit_state']) &&  $filters['audit_state'] != '') {
            $where[] = array(['jcp.audit_state', '=', trim($filters['audit_state'])]);
        }
        if (isset($filters['auth']) &&  $filters['auth'] != '') {
            $where[] = array(['jcp.auth', '=', trim($filters['auth'])]);
        }
        if (isset($filters['customer_id']) &&  $filters['customer_id'] != '') {
            $where[] = array(['jcp.customer_id', '=', trim($filters['customer_id'])]);
        }
        if (isset($filters['supplier_name']) &&  $filters['supplier_name'] != '') {
            $where[] = array(['js.title', 'like', '%' . trim($filters['supplier_name']) . '%']);
        }

        $list = Db::table('jy_customer_product jcp')
            ->field('jcp.id, jcp.product_name, jcp.product_model, jcp.product_type, jcp.week_start, jcp.week_end, 
            jcp.number, jcp.auth, jcp.audit_state, jcp.supplier_id, jcp.unit_type, jcp.unit, js.title, 
            ju.name as unit_str, js.title as supplier_name')
            ->join('jy_supplier js', 'jcp.supplier_id = js.id')
            ->leftJoin('jy_unit ju', 'jcp.unit = ju.id')
            ->where($where)
            ->where('is_del', self::IS_DEL_NOT_DEL)
            ->order('jcp.id', 'desc')
            ->paginate(['list_rows' => $page_size, 'page' => $page_index]);

        return $list;
    }

    /**
     * addCustomerProduct  新增我是采购者的供应产品
     * @param $data
     * @return int|string
     */
    public static function addCustomerProduct($data) {
        $add = Db::table('jy_customer_product')->insert($data);

        return $add;
    }

    /**
     * addCustomerProductAndGetId  新增我是采购者的产品并获取主键id
     * @param $data
     * @return int|string
     */
    public static function addCustomerProductAndGetId($data) {
        $add = Db::table('jy_customer_product')->insertGetId($data);

        return $add;
    }

    /**
     * editCustomerProduct 编辑我是采购者的供应产品
     *
     * @param $data
     * @return $edit
     */
    public static function editCustomerProduct($data) {
        $edit = Db::table('jy_customer_product')->where('id', (int)$data['id'])->update($data);

        return $edit;
    }

    /**
     * getDataById 获取采购端客户产品详情（通过主键id来获取）
     *
     * @param $id
     * @return $list
     */
    public static function getDataById($id) {
        $list = Db::table('jy_customer_product jcp')
            -> where('jcp.id', (int)$id)
            -> find();

        return $list;
    }

    /**
     * delCustomerProduct 删除产品
     *
     * @param $data
     * @return $del
     */
    public static function delCustomerProduct($data) {
        $del = Db::table('jy_customer_product')->where('id', (int)$data['id'])->update($data);

        return $del;
    }

    /**
     * updateState 供应商产品启用/禁用  //todo 这个方法不应该写在这个model中
    * @param $id
     * @param $state
     * @return int
     * @throws \think\db\exception\DbException
     */
    public static function updateState($id, $state) {
        $update_state = Db::table('jy_supplier')->where('id', (int)$id)->update(['state' => $state]);

        return $update_state;
    }

    /**
     * updateAuditState 供应商产品审核状态字段变更 编辑中/删除/未填报/待审核/已审核  可以修改成为这五种状态
     * @param $id
     * @param $audit_state
     * @return int
     * @throws \think\db\exception\DbException
     */
    public static function updateAuditState($id, $audit_state) {
        $update_state = Db::table('jy_customer_product')->where('id', (int)$id)->update(['audit_state' => $audit_state]);

        return $update_state;
    }

    /**
     * getNotVerifyListBySupplerId  根据供应商id获取客户要求填报的产品信息列表
     *
     * @param $supplier_id
     * @return array|\think\Collection|Db[]
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function getNotVerifyListBySupplerId($supplier_id, $product_name, $customer_name) {
        $where = array();
        if (isset($product_name) && $product_name != '') {
            $where[] = array(['jcp.product_name', 'like', '%' . trim($product_name) . '%']);
        }
//        $filters['customer_name'] = '供应商A'; //todo
        //先通过采购客户姓名查询到 供应商id 再进行查询
        if (isset($customer_name) &&  $customer_name != '') {
            $where[] = array(['jc.title', 'like', '%' . trim($customer_name) . '%']);
        }

        $list = Db::table('jy_customer_product jcp')
            ->field('jc.title, jcp.id as customer_product_id, jcp.product_name, jc.id as customer_id')
            ->join('jy_customer jc', 'jcp.customer_id = jc.id')
            -> where('jcp.supplier_id', (int)$supplier_id)
            -> where($where)
            -> select()
            ->toArray();

        return $list;
    }


}