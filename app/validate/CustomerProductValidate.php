<?php


namespace app\validate;


use think\Validate;

class CustomerProductValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'product_name'                  => 'require',
        'product_model'                      => 'require',
        'product_type'                      => 'require',
        'supplier_id'                      => 'require',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
        'product_name.require'          => '产品名称不能为空',
        'product_model.require'          => '产品型号不能为空',
        'product_type.require'          => '产品类型不能为空',
        'supplier_id.require'          => '供应商不能为空',
    ];
}