<?php
declare (strict_types = 1);

namespace app\validate;

use think\Validate;

class ProductValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'product_name'          => 'require|length:2,20',
        'product_no'            => 'require|length:2,20',
        'product_spec'          => 'length:2,20',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
        'product_name.require'          => '产品名称不能为空',
        'product_name.length'           => '产品名称长度需在2-20个字符之间',
        'product_no.require'            => '产品编码不能为空',
        'product_no.length'             => '产品编码长度需在2-20个字符之间',
        'product_spec.length'           => '产品规格长度需在2-20个字符之间',
    ];
}
