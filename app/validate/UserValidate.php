<?php
declare (strict_types = 1);

namespace app\validate;

use think\Validate;

class UserValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'username'          => 'require|length:2,20',
        'password'          => 'require|length:6,20',
        'telephone'         => 'mobile',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
        'username.require'          => '用户姓名不能为空',
        'username.length'           => '用户姓名长度需在2-20个字符之间',
        'password.require'          => '密码不能为空',
        'password.length'           => '密码长度需在6-20个字符之间',
        'telephone.mobile'          => '手机号无效',
    ];
}
