<?php


namespace app\api\controller\product;


use app\BaseController;
use app\model\product\ProductCalculateModel;
use app\model\product\ProductCarbonLabelModel;
use app\model\product\ProductModel;
use app\service\ProductCarbonLabelService;
use app\validate\ProductCarbonLabelValidate;
use think\exception\ValidateException;
use think\facade\Config;
use think\Request;


class ProductCarbonLabel extends BaseController
{
    /**
     * @notes 获取二维码地址
     * @author fengweizhe
     */
    public function getQrcodeUrl() {
        $id = isset($_GET['id']) ? $_GET['id'] : '';
        if ($id == '' || $id == null || $id == 0) {
            return json(['code'=>201, 'message'=>'参数id错误']);
        }
        //查询表中是否有qrcode名称 如果没有写入 有的话更新
        $info = ProductCarbonLabelModel::getDataById($id);

        if ($info['qrcode_name'] == null) {
            $url = Config::get('api.host.test').'/api/productcarbonlabelinfo?id='.$id;
            $qrcode_name = qrcode($url);
            ProductCarbonLabelModel::editProductCarbonLabel(['id'=>$id, 'qrcode_name'=>$qrcode_name]);
        } else {
            $qrcode_name = $info['qrcode_name'];
        }
        $url = Config::get('api.host.test').'/static/qrcode/'.$qrcode_name;
        $data['code'] = 200;
        $data['data']['url'] = $url;
        return json($data);//14.4  30
    }

    /**
     * @notes 碳中和标签列表
     * @author fengweizhe
     */
    public function index() {
        $page_size = isset($_GET['pageSize']) ? $_GET['pageSize'] : ProductCarbonLabelModel::pageSize;
        $page_index = isset($_GET['pageIndex']) ? $_GET['pageIndex'] : ProductCarbonLabelModel::pageIndex;
        $filter_product_name = isset($_GET['filter_product_name']) ? $_GET['filter_product_name'] : '';

        $list = ProductCarbonLabelService::getList($page_size, $page_index, ['filter_product_name'=>$filter_product_name]);

        $data['code'] = 200;
        $data['data']['list'] = $list['data'];
        $data['data']['total'] = $list['total'];

        return json($data);
    }
    //
    /**
     * @notes 碳中和标签新增表单
     * @author fengweizhe
     */
    public function addForm() {
        //产品信息
        $list = ProductModel::getCarconLabelAddFormProduct();
        //根据产品id数据 查询出产品的核算时间
        $product_week_date_info = ProductCalculateModel::getCalculateWeekByProdectIds(array_column($list, 'id'));
        $product_week_arr_data = [];
        foreach ($product_week_date_info as $k => $v) {
            $product_week_arr_data[$v['product_id']] = $v;
        }
        //把核算时间加入到返回的信息中
        $return_list = [];
        foreach ($list as $kk => $vv) {
            if (isset($product_week_arr_data[$vv['id']])) {
                $vv['week_date'][] = $product_week_arr_data[$vv['id']];
            } else {
                $vv['week_date'] = [];
            }
            $return_list[$kk] = $vv;
        }

        $data['code'] = 200;
        $data['data'] = $return_list;

        return json($data);
    }

    /**
     * @title 碳中和新增
     * @author fengweizhe
     */
    public function add(Request $request) {
        if (request()->isPost() && $this->validateForm() === true) {
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);
            $data_redis = $request->middleware('data_redis');
            $data['carbon_label_name'] = $params_payload_arr['carbon_label_name'];
            $data['product_id'] = $params_payload_arr['product_id'];
            $data['week_start'] = $params_payload_arr['week_start'];
            $data['week_end'] = $params_payload_arr['week_end'];
            $data['introduce'] = isset($params_payload_arr['introduce']) ? $params_payload_arr['introduce'] : '';
            $data['url'] = isset($params_payload_arr['url']) ? $params_payload_arr['url'] : '';
            $data['create_by'] = $data_redis['userid'];
            $data['modify_by'] = $data_redis['userid'];
            $data['create_time'] = date('Y-m-d H:i:s');
            $data['modify_time'] = date('Y-m-d H:i:s');
            $data['is_del'] = ProductCarbonLabelModel::IS_DEL_NOT_DEL;
            $product_info = ProductModel::getDataById($data['product_id']);
            $data['product_name'] = '';
            if ($product_info != null) {
                $data['product_name'] = $product_info['product_name'];
            }

            //获取product_calculate_id产品核算id
            $product_calculate_info = ProductCalculateModel::getCalculateByProuctIdAndWeekDate($data['product_id'], $data['week_start'], $data['week_end']);
            if ($product_calculate_info == null) {
                return json(['code'=>201, 'message'=>"未查询到产品的核算信息"]);
            }
            $data['product_calculate_id'] = $product_calculate_info['id'];
            // 添加操作日志
//            $data_log['user_id'] = $data['create_by'];
//            $data_log['menu_id'] = '8';// $_POST['menu_id']; // T.B.D 前端未传先注释
//            $data_log['time'] = $data['create_time'];
//            $data_log['url'] = 'product/add';// $_POST['url']; // T.B.D 前端未传先注释
//            $data_log['log'] = '添加产品：' . $data['product_name'];
//            $db = new OperationModel();
//            $db->addOperation($data_log);

            $add = ProductCarbonLabelModel::addProductCarbonLabel($data);

            if ($add) {
                $datasmg = ['code'=>200, 'message'=>"添加成功"];
            } else {
                $datasmg = ['code'=>201, 'message'=>"添加失败"];
            }
        } else {
            $datasmg = ['code'=>404, 'message'=>$this->validateForm()];
        }

        return json($datasmg);
    }

    /**
     * @title 碳中和删除
     * @author fengweizhe
     */
    public function del(Request $request) {
//        $data_redis = $request->middleware('data_redis');
//        $data['product_name'] = '示例产品A'; // $_POST['product_name']; // T.B.D 暂不确定->是否为核算名称+核算年份
//        // 添加操作日志
//        $data['user_id'] = $data_redis['userid'];
//        $data_log['menu_id'] = '8';// $_POST['menu_id']; // T.B.D 前端未传先注释
//        $data_log['time'] = date('Y-m-d H:i:s');
//        $data_log['url'] = 'productcalculate/del';// $_POST['url']; // T.B.D 前端未传先注释
//        $data_log['log'] = '删除产品核算：' . $data['product_name'];
//
//        $db = new OperationModel();
//        $db->addOperation($data_log);
        $params_payload = $this->req_payload;
        $params_payload_arr = json_decode($params_payload, true);
        $data['id'] = isset($params_payload_arr['id']) ? $params_payload_arr['id'] : '';
        if ($data['id'] == '' || $data['id'] == null || $data['id'] == 0) {
            return json(['code'=>201, 'message'=>"参数id错误"]);
        }
        $data['is_del'] = ProductCarbonLabelModel::IS_DEL_DEL;

        ProductCarbonLabelModel::delProductCarbonLabel($data);

        return json(['code'=>200, 'message'=>"删除成功"]);
    }

    /**
     * @title 碳中和编辑
     * @author fengweizhe
     */
    public function edit(Request $request) {
        if (request()->isPost() && $this->validateForm() === true) {
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);
            $data_redis = $request->middleware('data_redis');
            $data['id'] = $params_payload_arr['id'];
            $data['carbon_label_name'] = $params_payload_arr['carbon_label_name'];
            $data['product_id'] = $params_payload_arr['product_id'];
            $data['week_start'] = $params_payload_arr['week_start'];
            $data['week_end'] = $params_payload_arr['week_end'];
            $data['introduce'] = isset($params_payload_arr['introduce']) ? $params_payload_arr['introduce'] : '';
            $data['url'] = isset($params_payload_arr['url']) ? $params_payload_arr['url'] : '';
            $data['modify_by'] = $data_redis['userid'];
            $data['modify_time'] = date('Y-m-d H:i:s');
            $product_info = ProductModel::getDataById($data['product_id']);
            $data['product_name'] = '';
            if ($product_info != null) {
                $data['product_name'] = $product_info['product_name'];
            }
            //获取product_calculate_id产品核算id
            $product_calculate_info = ProductCalculateModel::getCalculateByProuctIdAndWeekDate($data['product_id'], $data['week_start'], $data['week_end']);
            if ($product_calculate_info == null) {
                return json(['code'=>201, 'message'=>"未查询到产品的核算信息"]);
            }
            $data['product_calculate_id'] = $product_calculate_info['id'];
            // 添加操作日志
//            $data_log['user_id'] = $data['modify_by'];
//            $data_log['menu_id'] = '8';// $_POST['menu_id']; // T.B.D 前端未传先注释
//            $data_log['time'] = $data['modify_time'];
//            $data_log['url'] = 'productcalculate/edit';// $_POST['url']; // T.B.D 前端未传先注释
//            $data_log['log'] = '编辑产品核算：' . $_POST['product_name'];
//            $db = new OperationModel();
//            $db->addOperation($data_log);

            ProductCarbonLabelModel::editProductCarbonLabel($data);
            $datasmg = ['code'=>200, 'message'=>"编辑成功"];
        } else {
            $datasmg = ['code'=>404, 'message'=>$this->validateForm()];
        }

        return json($datasmg);
    }

    /**
     * @title 碳中和二维码详情页
     * @author fengweizhe
     */
    public function info() {
        $id = isset($_GET['id']) ? $_GET['id'] : '';
        $product_carbon_label_info = ProductCarbonLabelModel::getDataById($id);
        if ($product_carbon_label_info == null) {
            return json(['code'=>201, 'message'=>"未查询到碳标签相关信息"]);
        }
        $week_start = $product_carbon_label_info['week_start']; //todo 临时写死 为了便于联调
        $week_end = $product_carbon_label_info['week_end'];
        $product_id = $product_carbon_label_info['product_id'];

        //根据参数查询看是哪个产品核算
        $info = ProductCalculateModel::getCalculateByProuctIdAndWeekDate($product_id, $week_start, $week_end);

        if ($info == null) {
            return json(['code'=>201, 'message'=>"未查询到核算的相关信息"]);
        }

        $return = ProductCarbonLabelService::getInfo($info['id'], $product_carbon_label_info['introduce'], $product_carbon_label_info['product_name']);

        $data['code'] = 200;
        $data['data'] = $return;

        return json($data);
    }

    /**
     * validateForm 验证
     *
     * @return void
     */
    protected function validateForm() {
        $data = request()->param();

        try {
            validate(ProductCarbonLabelValidate::class)->check($data);

            return  true;
        } catch (ValidateException $e) {

            return $e->getError();
        }
    }
}