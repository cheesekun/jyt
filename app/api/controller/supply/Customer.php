<?php


namespace app\api\controller\supply;


use app\BaseController;
use app\model\admin\IndustryModel;
use app\model\supply\SupplierModel;
use think\Request;

class Customer extends BaseController
{
    /**
     * 我是采购者  查看的是供应商的信息
     */

    /**
     * @notes 客户供应商的列表仓配OMS 01月16日 09.30-11.30 V1.6.0版本上线
     * @author fengweizhe
     */
    public function index() {
        $title = isset($_GET['title']) ? $_GET['title'] : '';
        $state = isset($_GET['state']) ? $_GET['state'] : '';
        $risk_state = isset($_GET['risk_state']) ? $_GET['risk_state'] : '';
        $industry_id = isset($_GET['industry_id']) ? $_GET['industry_id'] : '';
        $page_size = isset($_GET['pageSize']) ? $_GET['pageSize'] : SupplierModel::pageSize;
        $page_index = isset($_GET['pageIndex']) ? $_GET['pageIndex'] : SupplierModel::pageIndex;
        $main_id = 1; //todo 这里后面要从 session中获取
        $list = SupplierModel::getList($page_size, $page_index, ['title'=>$title, 'state'=>$state,
            'risk_state'=>$risk_state, 'industry_id'=>$industry_id, 'main_id'=>$main_id])->toArray();

        $industry_id_map = IndustryModel::getList();
        $state_select_map = SupplierModel::STATE_SELECT_MAP;
        $risk_state_select_map = SupplierModel::RISK_STATE_SELECT_MAP;
        $state_map = SupplierModel::STATE_MAP;
        $risk_state_map = SupplierModel::RISK_STATE_MAP;

        $list_new = [];
        foreach ($list['data'] as $k => $v) {
            if (isset($state_map[$v['state']])) {
                $v['state_name'] = $state_map[$v['state']];
            } else {
                $v['state_name'] = '';
            }
            if (isset($risk_state_map[$v['risk_state']])) {
                $v['risk_state_name'] = $risk_state_map[$v['risk_state']];
            } else {
                $v['risk_state_name'] = '';
            }
            $list_new[$k] = $v;
        }
        unset($list['data']);

        $data['code'] = 200;
        $data['data']['list'] = $list_new;
        $data['data']['industry_id'] = $industry_id_map;
        $data['data']['state'] = $state_select_map;
        $data['data']['risk_state'] = $risk_state_select_map;
        $data['data']['total'] = $list['total'];
        return json($data);
    }

    /**
     * @notes 客户供应商的详情

     * @author fengweizhe
     */
    public function info() {
        $id = isset($_GET['id']) ? $_GET['id'] : '';
        $info = SupplierModel::getDataById($id);
        if ($info == null) {
            return json(['code'=>201, 'message'=>"未查询到客户供应商相关信息"]);
        }
        $state_map = SupplierModel::STATE_MAP;
        $risk_state_map = SupplierModel::RISK_STATE_MAP;

        $info_new = [];
        foreach ($info as $k => $v) {
            if ($k == 'state' ) {
                $v = $state_map[$v];
            }
            if ($k == 'risk_state') {
                $v = $risk_state_map[$v];
            }
            $info_new[$k] = $v;
        }
        unset($info);
        $data['code'] = 200;
        $data['data'] = $info_new;

        return json($data);
    }

    /**
     * @notes 客户供应商的启用/禁用
     * @author fengweizhe
     */
    public function state(Request $request) {
        $params_payload = $this->req_payload;
        $params_payload_arr = json_decode($params_payload, true);
        $data['id'] = isset($params_payload_arr['id']) ? $params_payload_arr['id'] : '';
        if ($data['id'] == '' || $data['id'] == null || $data['id'] == 0) {
            return json(['code'=>201, 'message'=>"参数id错误"]);
        }
        $data['state'] = isset($params_payload_arr['state']) ? $params_payload_arr['state'] : '';
        if ($data['state'] == '' || $data['state'] == null || $data['state'] == 0) {
            return json(['code'=>201, 'message'=>"参数state错误"]);
        }
        if (!in_array($data['state'], [SupplierModel::STATE_YES, SupplierModel::STATE_NO])) {
            return json(['code'=>201, 'message'=>"参数state错误"]);
        }
        SupplierModel::updateState($data['id'], $data['state']);
        if ($data['state'] == SupplierModel::STATE_YES) {
            return json(['code'=>200, 'message'=>"启用成功"]);
        } else {
            return json(['code'=>200, 'message'=>"禁用成功"]);
        }
    }


    /**
     * @notes 客户供应商风险状态调整表单获取
     * @return \think\response\Json
     */
    public function riskStateMap() {
        $risk_state_select_map = SupplierModel::RISK_STATE_SELECT_MAP;
        $data['code'] = 200;
        $data['data'] = $risk_state_select_map;

        return json($data);
    }

    /**
     * @notes 客户供应商的风险状态调整
     * @author fengweizhe
     */
    public function riskState() {
        $params_payload = $this->req_payload;
        $params_payload_arr = json_decode($params_payload, true);
        $data['id'] = isset($params_payload_arr['id']) ? $params_payload_arr['id'] : '';
        if ($data['id'] == '' || $data['id'] == null || $data['id'] == 0) {
            return json(['code'=>201, 'message'=>"参数id错误"]);
        }
        $data['risk_state'] = isset($params_payload_arr['risk_state']) ? $params_payload_arr['risk_state'] : '';
        if ($data['risk_state'] == '' || $data['risk_state'] == null || $data['risk_state'] == 0) {
            return json(['code'=>201, 'message'=>"参数risk_state错误"]);
        }
        SupplierModel::updateRiskState($data['id'], $data['risk_state']);
        return json(['code'=>200, 'message'=>"风险等级变更成功"]);
    }

}