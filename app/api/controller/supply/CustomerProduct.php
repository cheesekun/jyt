<?php


namespace app\api\controller\supply;


use app\BaseController;
use app\model\admin\FileModel;
use app\model\supply\CustomerModel;
use app\model\supply\CustomerProductDischargeModel;
use app\model\supply\CustomerProductModel;
use app\model\supply\FactorTypeModel;
use app\model\supply\SupplierFillProductCustomerRelationModel;
use app\model\supply\SupplierModel;
use app\validate\CustomerProductValidate;
use think\exception\ValidateException;
use think\Request;

class CustomerProduct extends BaseController
{
    /**
     * 查看的是采购者的产品信息
     */

    /**
     * @notes 客户产品列表
     * @author fengweizhe
     */
    public function index() {
        $product_name = isset($_GET['product_name']) ? $_GET['product_name'] : '';
        $product_type = isset($_GET['product_type']) ? $_GET['product_type'] : '';
        $supplier_name = isset($_GET['supplier_name']) ? $_GET['supplier_name'] : '';
        $audit_state = isset($_GET['audit_state']) ? $_GET['audit_state'] : '';
        $auth = isset($_GET['auth']) ? $_GET['auth'] : '';
        $customer_id = 1;//todo 这里客户id先这样写死 后面要改成从 登录态中取出来
        $page_size = isset($_GET['pageSize']) ?$_GET['pageSize'] : CustomerProductModel::pageSize;
        $page_index = isset($_GET['pageIndex']) ? $_GET['pageIndex'] : CustomerProductModel::pageIndex;
        $list = CustomerProductModel::getList($page_size, $page_index, ['product_name'=>$product_name, 'product_type'=>$product_type,
            'supplier_name'=>$supplier_name, 'audit_state'=>$audit_state, 'auth'=>$auth, 'customer_id'=>$customer_id])->toArray();

        $audit_state_map = CustomerProductModel::AUDIT_STATE_MAP;
        $auth_map = CustomerProductModel::AUTH_MAP;
        $list_new = [];
        foreach ($list['data'] as $k => $v) {
            if (isset($audit_state_map[$v['audit_state']])) {
                $v['audit_state_name'] = $audit_state_map[$v['audit_state']];
            } else {
                $v['audit_state_name'] = '';
            }
            if (isset($auth_map[$v['auth']])) {
                $v['auth_name'] = $auth_map[$v['auth']];
            } else {
                $v['auth_name'] = '';
            }
            //carbon_coefficient  carbon_value
            $v['carbon_coefficient'] = 11; //todo 碳排放系数可以直接计算好存入表中 也可以在列表中计算出来 还是计算好存入表中吧 这里暂时先加个字段让前端对接
            $v['carbon_value'] = 11; //todo 排放量可以直接计算好存入表中 也可以在列表中计算出来 还是计算好存入表中吧 这里暂时先加个字段让前端对接
            $list_new[$k] = $v;
        }
        unset($list['data']);
        $data['code'] = 200;
        $data['data']['list'] = $list_new;
        $data['data']['audit_state'] = CustomerProductModel::AUDIT_STATE_SELECT_MAP;
        $data['data']['auth'] = CustomerProductModel::AUTH_SELECT_MAP;
        $data['data']['total'] = $list['total'] ;
        return json($data);
    }

    /**
     * @notes 客户产品列表-产品详情
     * @author fengweizhe
     */
    public function info() {
        $id = isset($_GET['id']) ? $_GET['id'] : '';
//        $id = 1;//todo
        $customer_product_info = CustomerProductModel::getDataById($id);
        if ($customer_product_info == null) {
            return json(['code'=>201, 'message'=>"未查询到相客户产品相关信息"]);
        }

        //根据产品id去供应产品与客户关系表中查询对应客户
        $supplier_name = SupplierModel::getTitleById($customer_product_info['id']);
        $customer_product_info['supplier_name'] = $supplier_name;
        //todo  碳排放系数 写死
        $customer_product_info['carbon_coefficient'] = 11; //todo 碳排放系数可以直接计算好存入表中 也可以在列表中计算出来 还是计算好存入表中吧 这里暂时先加个字段让前端对接
        $customer_product_info['carbon_value'] = 11; //todo 排放量可以直接计算好存入表中 也可以在列表中计算出来 还是计算好存入表中吧 这里暂时先加个字段让前端对接
        //把文件的相关信息补全
        $file_url = $customer_product_info['file_url'];
        $customer_product_info['files'] = [];
        if ($file_url != '') {
            $file_url_arr = explode(',', $file_url);
            foreach ($file_url_arr as $k => $v) {
                $file_data = FileModel::getInfoById($v);
                $customer_product_info['files'][] = $file_data;
            }
        }
        //根据产品id查询碳核算信息
        $discharge_info = CustomerProductDischargeModel::getDataByProductId($customer_product_info['id']);
        $type_id_arr = array_column($discharge_info, 'type_id');
        $type_id_arr = array_unique($type_id_arr);
        $factor_map = FactorTypeModel::getList();
        $factor_map_new = [];
        foreach ($factor_map as $k => $v) {
            $factor_map_new[$v['id']] = $v['name'];
        }
        //封装排放源结构
        $discharge_info_new = [];
        foreach ($type_id_arr as $kk => $vv) {
            $arr['type_name'] = $factor_map_new[$vv];
            $tmp = [];
            foreach ($discharge_info as $kkk => $vvv) {
                $vvv['unit'] = (float)$vvv;
                $vvv['unit_type'] = (float)$vvv;
                if ($vvv['type_id'] == $vv) {
                    $tmp[] = $vvv;
                }
            }
            $arr['datas'] = $tmp;
            $discharge_info_new[] = $arr;
        }

        if (!empty($discharge_info)) {
            $customer_product_info['discharge'] = $discharge_info_new;
        } else {
            $customer_product_info['discharge'] = [];
        }

        $data['code'] = 200;
        $data['data'] = $customer_product_info;
        return json($data);
    }

    /**
     * @notes 客户产品新增
     * @author fengweizhe
     */
    public function add(Request $request) {
        if (request()->isPost() && $this->validateForm() === true) {
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);
            $data_redis = $request->middleware('data_redis');
            $data['product_name'] = $params_payload_arr['product_name'];
            $data['product_model'] = $params_payload_arr['product_model'];
            $data['product_type'] = $params_payload_arr['product_type'];
            $data['supplier_id'] = $params_payload_arr['supplier_id'];
            $data['create_by'] = isset($data_redis['userid']) ? $data_redis['userid'] : '';
            $data['modify_by'] = isset($data_redis['userid']) ? $data_redis['userid'] : '';
            $data['create_time'] = date('Y-m-d H:i:s');
            $data['modify_time'] = date('Y-m-d H:i:s');
            $add = CustomerProductModel::addCustomerProduct($data);
            //todo 这里还得加一个customer_id来看是谁新增的这个产品 这个字段要有得 后面权限及查询该客户得产品列表都要用到得
            if ($add) {
                $datasmg = ['code'=>200, 'message'=>"添加成功"];
            } else {
                $datasmg = ['code'=>201, 'message'=>"添加失败"];
            }
        } else {
            $datasmg = ['code'=>404, 'message'=>$this->validateForm()];
        }

        return json($datasmg);
    }

    /**
     * @notes 客户产品编辑
     * @author fengweizhe
     */
    public function edit(Request $request) {
        if (request()->isPost() && $this->validateForm() === true) {
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);
            $data_redis = $request->middleware('data_redis');
            $data['id'] = $params_payload_arr['id'];
            $data['product_name'] = $params_payload_arr['product_name'];
            $data['product_model'] = $params_payload_arr['product_model'];
            $data['product_type'] = $params_payload_arr['product_type'];
            $data['supplier_id'] = $params_payload_arr['supplier_id'];
            $data['modify_by'] = isset($data_redis['userid']) ? $data_redis['userid'] : '';
            $data['create_time'] = date('Y-m-d H:i:s');
            $data['modify_time'] = date('Y-m-d H:i:s');
            CustomerProductModel::editCustomerProduct($data);
            $datasmg = ['code'=>200, 'message'=>"编辑成功"];
        } else {
            $datasmg = ['code'=>404, 'message'=>$this->validateForm()];
        }

        return json($datasmg);
    }

    /**
     * @notes 客户产品删除
     * @author fengweizhe
     */
    public function del(Request $request) {
        $params_payload = $this->req_payload;
        $params_payload_arr = json_decode($params_payload, true);
        $data['id'] = isset($params_payload_arr['id']) ? $params_payload_arr['id'] : '';
        if ($data['id'] == '' || $data['id'] == null || $data['id'] == 0) {
            return json(['code'=>201, 'message'=>"参数id错误"]);
        }
        $data['is_del'] = CustomerProductModel::IS_DEL_DEL;
        CustomerProductModel::delCustomerProduct($data);
        return json(['code'=>200, 'message'=>"删除成功"]);
    }

    /**
     * @notes 客户产品启用/禁用
     * @author fengweizhe
     */
    public function state(Request $request) {
        $params_payload = $this->req_payload;
        $params_payload_arr = json_decode($params_payload, true);
        $data['id'] = isset($params_payload_arr['id']) ? $params_payload_arr['id'] : '';
        if ($data['id'] == '' || $data['id'] == null || $data['id'] == 0) {
            return json(['code'=>201, 'message'=>"参数id错误"]);
        }
        $data['state'] = isset($params_payload_arr['state']) ? $params_payload_arr['state'] : '';
        if ($data['state'] == '' || $data['state'] == null || $data['state'] == 0) {
            return json(['code'=>201, 'message'=>"参数state错误"]);
        }
        if (!in_array($data['state'], [CustomerProductModel::STATE_YES, CustomerProductModel::STATE_NO])) {
            return json(['code'=>201, 'message'=>"参数state错误"]);
        }
        if ($data['state'] == CustomerProductModel::STATE_YES) {
            $data['audit_state'] = CustomerProductModel::AUDIT_STATE_VERIFY;
        } else {
            $data['audit_state'] = CustomerProductModel::AUDIT_STATE_NOT_USE;
        }
        CustomerProductModel::updateState($data['id'], $data['state']);
        CustomerProductModel::updateAuditState($data['id'], $data['audit_state']);
        if ($data['state'] == CustomerProductModel::STATE_YES) {
            return json(['code'=>200, 'message'=>"启用成功"]);
        } else {
            return json(['code'=>200, 'message'=>"禁用成功"]);
        }
    }

    /**
     * @notes 客户产品excel模板下载
     * @author fengweizhe
     */
    public function templet() {

    }

    /**
     * @notes 客户产品excel上传
     * @author fengweizhe
     */
    public function upload() {

    }

    /**
     * @notes  报送审批列表
     * @author fengweizhe
     */
    public function auditList() {
        $product_name = isset($_GET['product_name']) ? $_GET['product_name'] : '';
        $supplier_name = isset($_GET['supplier_name']) ? $_GET['supplier_name'] : '';
        $audit_state = CustomerProductModel::AUDIT_STATE_NOT_VERIFY;
        $customer_id = 1;//todo 这里客户id先这样写死 后面要改成从 登录态中取出来
        $page_size = isset($_GET['pageSize']) ?$_GET['pageSize'] : CustomerProductModel::pageSize;
        $page_index = isset($_GET['pageIndex']) ? $_GET['pageIndex'] : CustomerProductModel::pageIndex;
        $list = CustomerProductModel::getList($page_size, $page_index, ['product_name'=>$product_name,
            'supplier_name'=>$supplier_name, 'audit_state'=>$audit_state, 'customer_id'=>$customer_id])->toArray();
        $list_new = [];
        foreach ($list['data'] as $k => $v) {
            $v['carbon_coefficient'] = 11;
            $list_new[$k] = $v;
        }
        $data['code'] = 200;
        $data['data']['list'] = $list_new;
        $data['data']['total'] = $list['total'] ;
        return json($data);
    }

    /**
     * @notes 客户产品审核/驳回
     * @author fengweizhe
     */
    public function auditState() {
        $params_payload = $this->req_payload;
        $params_payload_arr = json_decode($params_payload, true);
        $data['id'] = isset($params_payload_arr['id']) ? $params_payload_arr['id'] : '';
        if ($data['id'] == '' || $data['id'] == null || $data['id'] == 0) {
            return json(['code'=>201, 'message'=>"参数id错误"]);
        }
        $data['audit_state'] = isset($params_payload_arr['audit_state']) ? $params_payload_arr['audit_state'] : '';
        if ($data['audit_state'] == '' || $data['audit_state'] == null || $data['audit_state'] == 0) {
            return json(['code'=>201, 'message'=>"参数audit_state错误"]);
        }
        if (!in_array($data['audit_state'], [CustomerProductModel::AUDIT_STATE_VERIFY, CustomerProductModel::AUDIT_STATE_EDITING])) {
            return json(['code'=>201, 'message'=>"参数audit_state错误"]);
        }
        CustomerProductModel::updateAuditState($data['id'], $data['audit_state']);
        if ($data['audit_state'] == CustomerProductModel::AUDIT_STATE_VERIFY) {
            return json(['code'=>200, 'message'=>"审核成功"]);
        } else {
            return json(['code'=>200, 'message'=>"驳回成功"]);
        }
    }

    /**
     * @notes 客户产品查看供应商详情  todo 这个有其他接口 可以直接用 就不需要再写一遍了
     * @author fengweizhe
     */
//    public function supplierInfo() {
//
//    }

    /**
     * @notes 客户产品要求填报
     * @author fengweizhe
     */
    public function requestFill() {
        $params_payload = $this->req_payload;
        $params_payload_arr = json_decode($params_payload, true);
        $data['id'] = isset($params_payload_arr['id']) ? $params_payload_arr['id'] : '';
        if ($data['id'] == '' || $data['id'] == null || $data['id'] == 0) {
            return json(['code'=>201, 'message'=>"参数id错误"]);
        }
        $data['audit_state'] = isset($params_payload_arr['audit_state']) ? $params_payload_arr['audit_state'] : '';
        if ($data['audit_state'] == '' || $data['audit_state'] == null || $data['audit_state'] == 0) {
            return json(['code'=>201, 'message'=>"参数audit_state错误"]);
        }
        if (!in_array($data['audit_state'], [CustomerProductModel::AUDIT_STATE_NOT_FILL])) {
            return json(['code'=>201, 'message'=>"参数audit_state错误"]);
        }
        CustomerProductModel::updateAuditState($data['id'], $data['audit_state']);

        return json(['code'=>200, 'message'=>"要求填报成功"]);

    }

    /**
     * @notes 获取产品添加时的表单信息
     *
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function addForm() {
        $main_id = 1; //todo 这里后面要从 session中获取
        $supplier_list = SupplierModel::getCustomerProductAddFormData($main_id);
        $data['code'] = 200;
        $data['data']['list'] = $supplier_list;
        return json($data);
    }

    /**
     * validateForm 验证
     *
     * @return void
     */
    protected function validateForm() {
        $data = request()->param();

        try {
            validate(CustomerProductValidate::class)->check($data);

            return  true;
        } catch (ValidateException $e) {

            return $e->getError();
        }
    }


}