<?php


namespace app\api\controller\supply;


use app\BaseController;
use app\model\admin\FileModel;
use app\model\admin\IndustryModel;
use app\model\supply\CustomerModel;
use app\model\supply\CustomerProductDischargeModel;
use app\model\supply\CustomerProductModel;
use app\model\supply\FactorTypeModel;
use app\model\supply\SupplierFillProductCustomerRelationModel;
use app\model\supply\SupplierProductDischargeModel;
use app\model\supply\SupplierProductModel;
use app\validate\SupplierProductValidate;

use think\exception\ValidateException;
use think\facade\Db;
use think\Request;

class SupplierProduct extends BaseController
{
    /**
     * 查看的是供应商的产品信息
     */

    /**
     * @notes 供应商产品列表
     * @author fengweizhe
     */
    public function index() {
        $product_name = isset($_GET['product_name']) ? $_GET['product_name'] : '';
        $product_type = isset($_GET['product_type']) ? $_GET['product_type'] : '';
        $customer_name = isset($_GET['customer_name']) ? $_GET['customer_name'] : '';
        $auth = isset($_GET['auth']) ? $_GET['auth'] : '';
        $supplier_id = 1;//todo 这里客户id先这样写死 后面要改成从 登录态中取出来 或者取出 user_id然后取供应商表中进行查询
        $page_size = isset($_GET['pageSize']) ?$_GET['pageSize'] : SupplierProductModel::pageSize;
        $page_index = isset($_GET['pageIndex']) ? $_GET['pageIndex'] : SupplierProductModel::pageIndex;
        $list = SupplierProductModel::getList($page_size, $page_index, ['product_name'=>$product_name, 'product_type'=>$product_type,
            'customer_name'=>$customer_name, 'auth'=>$auth, 'supplier_id'=>$supplier_id])->toArray();
        $list_new = [];
        $auth_map = SupplierProductModel::AUTH_MAP;
        //增加客户名称信息
        $product_ids = array_column($list['data'], 'id');
        $customer_name_info = SupplierFillProductCustomerRelationModel::getCustomerTitleBySupplyProductIds($product_ids);
        $customer_name_map = [];
        foreach ($customer_name_info as $ck => $cv) {
            $customer_name_map[$cv['supplier_product_id']][] = $cv['title'];
            $customer_name_map[$cv['supplier_product_id']] = array_unique($customer_name_map[$cv['supplier_product_id']]);
        }

        foreach ($list['data'] as $k => $v) {
            if (isset($auth_map[$v['auth']])) {
                $v['auth_name'] = $auth_map[$v['auth']];
            } else {
                $v['auth_name'] = '';
            }
            //客户名称
            $customer_name_arr = isset($customer_name_map[$v['id']]) ? $customer_name_map[$v['id']] : [];
            $customer_name_str = implode(',', $customer_name_arr);
            $v['customer_name'] = $customer_name_str;
            //carbon_coefficient  carbon_value
            $v['carbon_coefficient'] = 11; //todo 碳排放系数可以直接计算好存入表中 也可以在列表中计算出来 还是计算好存入表中吧 这里暂时先加个字段让前端对接
            $v['carbon_value'] = 11; //todo 排放量可以直接计算好存入表中 也可以在列表中计算出来 还是计算好存入表中吧 这里暂时先加个字段让前端对接

            $list_new[$k] = $v;
        }
        unset($list['data']);
        $data['code'] = 200;
        $data['data']['list'] = $list_new;
        $data['data']['total'] = $list['total'] ;
        $data['data']['auth'] = SupplierProductModel::AUTH_SELECT_MAP;
        return json($data);
    }

    /**
     * @notes 供应商产品列表
     * @author fengweizhe
     */
    public function info() {
        $id = isset($_GET['id']) ? $_GET['id'] : '';
        $supplier_product_info = SupplierProductModel::getDataById($id);
        if ($supplier_product_info == null) {
            return json(['code'=>201, 'message'=>"未查询到供应商产品相关信息"]);
        }
         //根据产品id去供应产品与客户关系表中查询对应客户
        $customer_name = SupplierFillProductCustomerRelationModel::getCustomerTitleBySupplyProductId($supplier_product_info['id']);
        $customer_name_arr = array_unique(array_column($customer_name, 'title'));
        $customer_name_str = implode(',', $customer_name_arr);
        $supplier_product_info['customer_name'] = $customer_name_str;
        //todo  碳排放系数 写死
        $supplier_product_info['carbon_coefficient'] = 11; //todo 碳排放系数可以直接计算好存入表中 也可以在列表中计算出来 还是计算好存入表中吧 这里暂时先加个字段让前端对接
        $supplier_product_info['carbon_value'] = 11; //todo 排放量可以直接计算好存入表中 也可以在列表中计算出来 还是计算好存入表中吧 这里暂时先加个字段让前端对接

        //把文件的相关信息补全
        $file_url = $supplier_product_info['file_url'];
        $supplier_product_info['files'] = [];
        if ($file_url != '') {
            $file_url_arr = explode(',', $file_url);
            foreach ($file_url_arr as $k => $v) {
                $file_data = FileModel::getInfoById($v);
                $supplier_product_info['files'][] = $file_data;
            }
        }
        //根据产品id查询碳核算信息
        $discharge_info = SupplierProductDischargeModel::getDataByProductId($supplier_product_info['id']);
        $type_id_arr = array_column($discharge_info, 'type_id');
        $type_id_arr = array_unique($type_id_arr);
        $factor_map = FactorTypeModel::getList();
        $factor_map_new = [];
        foreach ($factor_map as $k => $v) {
            $factor_map_new[$v['id']] = $v['name'];
        }
        //封装排放源结构
        $discharge_info_new = [];
        foreach ($type_id_arr as $kk => $vv) {
            $arr['type_name'] = $factor_map_new[$vv];
            $tmp = [];
            foreach ($discharge_info as $kkk => $vvv) {
                $vvv['unit_type'] = (float)$vvv;
                $vvv['unit'] = (float)$vvv;
                if ($vvv['type_id'] == $vv) {
                    $tmp[] = $vvv;
                }
            }
            $arr['datas'] = $tmp;
            $discharge_info_new[] = $arr;
        }



        if (!empty($discharge_info)) {
            $supplier_product_info['discharge'] = $discharge_info_new;
        } else {
            $supplier_product_info['discharge'] = [];
        }
        $data['code'] = 200;
        $data['data'] = $supplier_product_info;
        return json($data);


    }

    /**
     * @notes 供应商产品新增
     * @author fengweizhe
     */
    public function add(Request $request) {
        if (request()->isPost() && $this->validateForm() === true) {
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);
            $data_redis = $request->middleware('data_redis');
            $data['product_name'] = $params_payload_arr['product_name'];
            $data['product_model'] = $params_payload_arr['product_model'];
            $data['product_type'] = $params_payload_arr['product_type'];
            $data['auth'] = $params_payload_arr['auth'];
            $data['week_start'] = $params_payload_arr['week_start'];
            $data['week_end'] = $params_payload_arr['week_end'];
            $data['number'] = $params_payload_arr['number'];
            $data['unit_type'] = $params_payload_arr['unit_type'];
            $data['unit'] = $params_payload_arr['unit'];
            $data['file_url'] = isset($params_payload_arr['file_url']) ? $params_payload_arr['file_url'] : '';
            $data['supplier_id'] = 1; //todo 供应商id是从客户的登录态中取出客户id查询供应商表 或者 直接把客户id或者供应商id写到缓存中然后直接取都行 这里先空着   supplier_id需要这个字段来做区分是哪个供应商添加的产品
//            $data['customer_id'] = $params_payload_arr['customer_id']; //todo 这里的客户id就先空着  等下会后会有选择的地方
            $data['discharge'] = $params_payload_arr['discharge'];
            $data['create_by'] = isset($data_redis['userid']) ? $data_redis['userid'] : '';
            $data['modify_by'] = isset($data_redis['userid']) ? $data_redis['userid'] : '';
            $data['create_time'] = date('Y-m-d H:i:s');
            $data['modify_time'] = date('Y-m-d H:i:s');
            $discharge = $data['discharge'];
            unset($data['discharge']);
            //封装排放因子数据, 以及产品排放量和排放系数
            foreach ($discharge as $dk => &$dv) {
                //todo 这里排放系数的单位 和 排放量的单位如果不同 需要转换这里 这个逻辑后面再加上
                $dv['emissions'] = $dv['value'] * $dv['factor'];
            }

            //计算出产品的碳排放量
            $emissions_arr = array_column($discharge, 'emissions');
            $product_emissions = array_sum($emissions_arr);
            $data['emissions'] = $product_emissions;
            $data['coefficient'] = $product_emissions / $data['number'];

            Db::startTrans();
            try {
                $product_id = SupplierProductModel::addSupplierProduct($data);
                $discharge_insert = [];
                foreach ($discharge as $kk => $vv) {
                    $vv['supplier_product_id'] = $product_id;
                    $discharge_insert[$kk] = $vv;
                }
                SupplierProductDischargeModel::addSupplierProductDischarge($discharge_insert);
                Db::commit();
                $datasmg = ['code'=>200, 'message'=>"添加成功"];
            } catch (\Exception $e) {
                Db::rollback();
                $datasmg = ['code'=>201, 'message'=>$e->getMessage().$e->getLine()];
            }
        } else {
            $datasmg = ['code'=>404, 'message'=>$this->validateForm()];
        }

        return json($datasmg);
    }

    /**
     * @notes 供应商产品编辑
     * @author fengweizhe
     */
    public function edit(Request $request) {
        if (request()->isPost() && $this->validateForm() === true) {
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);
            $data_redis = $request->middleware('data_redis');
            $data['id'] = $params_payload_arr['id'];
            $data['product_name'] = $params_payload_arr['product_name'];
            $data['product_model'] = $params_payload_arr['product_model'];
            $data['product_type'] = $params_payload_arr['product_type'];
            $data['auth'] = $params_payload_arr['auth'];
            $data['week_start'] = $params_payload_arr['week_start'];
            $data['week_end'] = $params_payload_arr['week_end'];
            $data['number'] = (float)$params_payload_arr['number'];
            $data['unit_type'] = $params_payload_arr['unit_type'];
            $data['unit'] = $params_payload_arr['unit'];
            $data['file_url'] = isset($params_payload_arr['file_url']) ? $params_payload_arr['file_url'] : '';
            $data['supplier_id'] = 1; //todo 供应商id是从客户的登录态中取出客户id查询供应商表 或者 直接把客户id或者供应商id写到缓存中然后直接取都行 这里先空着   supplier_id需要这个字段来做区分是哪个供应商添加的产品
//            $data['customer_id'] = $params_payload_arr['customer_id']; //todo 这里的客户id就先空着  等下会后会有选择的地方
            $data['discharge'] = $params_payload_arr['discharge'];
            $data['create_by'] = isset($data_redis['userid']) ? $data_redis['userid'] : '';
            $data['modify_by'] = isset($data_redis['userid']) ? $data_redis['userid'] : '';
            $data['create_time'] = date('Y-m-d H:i:s');
            $data['modify_time'] = date('Y-m-d H:i:s');
            $discharge = $data['discharge'];
            unset($data['discharge']);
            //封装排放因子数据, 以及产品排放量和排放系数
            foreach ($discharge as $dk => &$dv) {
                //todo 这里排放系数的单位 和 排放量的单位如果不同 需要转换这里 这个逻辑后面再加上
                $dv['emissions'] = $dv['value'] * $dv['factor'];
            }
            //计算出产品的碳排放量
            $emissions_arr = array_column($discharge, 'emissions');
            $product_emissions = array_sum($emissions_arr);
            $data['emissions'] = $product_emissions;
            $data['coefficient'] = $product_emissions / $data['number'];

            Db::startTrans();
            try {
                SupplierProductModel::editSupplierProduct($data);
                $discharge_insert = [];
                foreach ($discharge as $kk => $vv) {
                    $vv['supplier_product_id'] = $data['id'];
                    $discharge_insert[$kk] = $vv;
                }
                //把排放源全部删除
                SupplierProductDischargeModel::delProductDischarge($data['id']);
                //重新新增排放源信息
                SupplierProductDischargeModel::addSupplierProductDischarge($discharge_insert);
                Db::commit();
                $datasmg = ['code'=>200, 'message'=>"编辑成功"];
            } catch (\Exception $e) {
                Db::rollback();
                $datasmg = ['code'=>201, 'message'=>$e->getMessage().$e->getLine()];
            }
        } else {
            $datasmg = ['code'=>404, 'message'=>$this->validateForm()];
        }

        return json($datasmg);
    }

    /**
     * @notes 供应商产品删除
     * @author fengweizhe
     */
    public function del(Request $request) {
        $params_payload = $this->req_payload;
        $params_payload_arr = json_decode($params_payload, true);
        $data['id'] = isset($params_payload_arr['id']) ? $params_payload_arr['id'] : '';
        if ($data['id'] == '' || $data['id'] == null || $data['id'] == 0) {
            return json(['code'=>201, 'message'=>"参数id错误"]);
        }
        //删除商品
        SupplierProductModel::delSupplierProduct($data['id']);
        //删除商品的排放源
        SupplierProductDischargeModel::delProductDischarge($data['id']);
        return json(['code'=>200, 'message'=>"删除成功"]);
    }

    /**
     * @notes 获取产品添加时的表单信息
     *
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function addForm() {
        $main_id = 1; //todo 这里后面要从 session中获取
        $supplier_list = CustomerModel::addForm($main_id);
        $data['code'] = 200;
        $data['data']['list'] = $supplier_list;
        return json($data);
    }

    /**
     * @notes 供应商产品启用/禁用
     * @author fengweizhe
     */
    public function state() {

    }

    /**
     * @notes 供应商产品excel模板下载
     * @author fengweizhe
     */
    public function templet() {

    }

    /**
     * @notes 供应商产品excel上传
     * @author fengweizhe
     */
    public function upload() {

    }

    /**
     * @notes 客户产品审核/驳回
     * @author fengweizhe
     */
    public function auditState() {

    }

    /**
     * @notes 客户产品查看供应商详情
     * @author fengweizhe
     */
    public function supplierInfo() {
//供应商报送产品客户关系表 jy_supplier_fill_product_customer_relation
        //supplier_product_id  customer_id id customer_product_id   audit_state    create_by
        //modify_by  create_time   modify_time
    }

    /**
     * @notes  客户报送列表
     * @author fengweizhe
     */
    public function supplierProductFillList(Request $request) {
        $data['id'] = isset($_GET['id']) ? $_GET['id'] : '';
        if ($data['id'] == '' || $data['id'] == null || $data['id'] == 0) {
            return json(['code'=>201, 'message'=>"参数id错误"]);
        }
        $data['product_name'] = isset($_GET['product_name']) ? $_GET['product_name'] : '';

        $data['carbon_coefficient'] = isset($_GET['carbon_coefficient']) ? $_GET['carbon_coefficient'] : '';

        $data['title'] = isset($_GET['title']) ? $_GET['title'] : '';
        $data['industry_id'] = isset($_GET['industry_id']) ? $_GET['industry_id'] : 0;
        //先通过公共商 查询出所有的相关联的客户的信息 列表中的客户信息就通过这里来查询出来
        $main = 11;//todo 这个id后面从session中取出来
        $customer_arr = CustomerModel::getAllDataById($main, $data['title'], $data['industry_id'] );
        //查询供应产品数量 以map形式来绑定每行数据
        $customer_id_arr = array_column($customer_arr, 'id');
        $customer_product_count_arr = SupplierProductModel::getProductCountByCustomerIdArrString($customer_id_arr);
        $customer_product_count_map = [];
        if (!empty($customer_product_count_arr)) {
            foreach ($customer_product_count_arr as $k => $v) {
                $customer_product_count_map[$v['customer_id']] = $v['count'];
            }
        }
        //查询报送表 看看有哪些客户报送了 以绑定的方式绑定已报送或者未报送 有客户id的就是已经报送的
        $id = $data['id'];
        $id = 5; //todo  供应产品id
        $relation = SupplierFillProductCustomerRelationModel::getDataBySupplierProductId($id);
        $relation_customer_id_arr = array_column($relation, 'customer_id');
        //进行数据整合
        $return_data = [];
        foreach ($customer_arr as $kk => $vv) {
            if (isset($customer_product_count_map[$vv['id']])) {
                $vv['supplier_product_count'] = $customer_product_count_map[$vv['id']];
            } else {
                $vv['supplier_product_count'] = 0;
            }
            if (in_array($vv['id'], $relation_customer_id_arr)) {
                $vv['fill_state'] = '已报送';
            } else {
                $vv['fill_state'] = '未报送';
            }
            $return_data[$kk] = $vv;
        }
        //行业map
        $industry_id_map = IndustryModel::getList();
        $data_json['code'] = 200;
        $data_json['data']['list'] = $return_data;
        $data_json['data']['industry_id_map'] = $industry_id_map;
        $data_json['data']['product_name'] = $data['product_name'];
        $data_json['data']['carbon_coefficient'] = $data['carbon_coefficient'];
        return json($data_json);

    }

    /**
     * @notes  客户报送列表-发起报送
     * @author fengweizhe
     */
    public function requestFill(Request $request) {
        //发起报送是把供应商的产品和排放源 分别查询出来 插入到客户的产品表 和 排放源表 然后最后把 带有客户表的产品id 存入到上报关系表中
        $params_payload = $this->req_payload;
        $params_payload_arr = json_decode($params_payload, true);
        $data['id'] = isset($params_payload_arr['id']) ? $params_payload_arr['id'] : '';
        if ($data['id'] == '' || $data['id'] == null || $data['id'] == 0) {
            return json(['code'=>201, 'message'=>"参数id错误"]);
        }
        $data['customer_id'] = isset($params_payload_arr['customer_id']) ? $params_payload_arr['customer_id'] : '';
        if ($data['customer_id'] == '' || $data['customer_id'] == null || $data['customer_id'] == 0) {
            return json(['code'=>201, 'message'=>"参数customer_id错误"]);
        }

        Db::startTrans();
        try {
            //查询供应商产品
            $product_info = SupplierProductModel::getDataById($data['id']);
            if ($product_info == null) {
                return json(['code'=>201, 'message'=>"未查询到供应商端产品信息"]);
            }
            //整合数据写入产品
            $insert_data['product_name'] = $product_info['product_name'];
            $insert_data['product_model'] = $product_info['product_model'];
            $insert_data['product_type'] = $product_info['product_type'];
            $insert_data['supplier_id'] = $product_info['supplier_id'];
            $insert_data['auth'] = $product_info['auth'];
            $insert_data['state'] = $product_info['state'];
            $insert_data['file_url'] = $product_info['file_url'];
            $insert_data['create_time'] = date('Y-m-d H:i:s');
            $insert_data['modify_time'] = date('Y-m-d H:i:s');
            $insert_data['customer_id'] = $data['customer_id'];
            $insert_data['audit_state'] = $product_info['audit_state'];
            $insert_data['week_start'] = $product_info['week_start'];
            $insert_data['week_end'] = $product_info['week_end'];
            $insert_data['number'] = $product_info['number'];
            $insert_data['unit_type'] = $product_info['customer_id'];
            $insert_data['unit'] = $product_info['unit'];
            $insert_data['emissions'] = $product_info['emissions'];
            $insert_data['coefficient'] = $product_info['coefficient'];
            $data_redis = $request->middleware('data_redis');
            $insert_data['create_by'] = isset($data_redis['userid']) ? $data_redis['userid'] : '';
            $insert_data['modify_by'] = isset($data_redis['userid']) ? $data_redis['userid'] : '';
            //整合数据写入排放源
            //TODO  这里加个逻辑 就是 那个型号+1的逻辑 这里别忘记了
            $product_add_back_id = CustomerProductModel::addCustomerProductAndGetId($insert_data);
            //查询排放源
            $discharge_info = SupplierProductDischargeModel::getDataByProductId($data['id']);
            $batch_insert_data = [];
            foreach ($discharge_info as $k => $v) {
                $v_new['type_id'] = $v['type_id'];
                $v_new['type_name'] = $v['type_name'];
                $v_new['name'] = $v['name'];
                $v_new['loss_type'] = $v['loss_type'];
                $v_new['value'] = $v['value'];
                $v_new['unit_type'] = $v['unit_type'];
                $v_new['unit'] = $v['unit'];
                $v_new['factor'] = $v['factor'];
                $v_new['factor_source'] = $v['factor_source'];
                $v_new['unit'] = $v['unit'];
                $v_new['emissions'] = $v['emissions'];
                $v_new['coefficient'] = $v['coefficient'];
                $v_new['customer_product_id'] = $product_add_back_id;
                $v_new['create_by'] = isset($data_redis['userid']) ? $data_redis['userid'] : '';
                $v_new['modify_by'] = isset($data_redis['userid']) ? $data_redis['userid'] : '';
                $v_new['create_time'] = date('Y-m-d H:i:s');
                $v_new['modify_time'] = date('Y-m-d H:i:s');
                $v_new['is_del'] = SupplierProductDischargeModel::IS_DEL_NOT_DEL;
                $batch_insert_data[$k] = $v_new;
            }
            if (!empty($batch_insert_data)) {
                CustomerProductDischargeModel::addCustomerProductDischarge($batch_insert_data);
            }
            //在供应商的关系表中,写入对应的关系信息 主要写入产品id 客户id 客户产品id
            $relation_insert_data = [
                'supplier_product_id' => $data['id'],
                'customer_id' => $data['customer_id'],
                'customer_product_id' => $product_add_back_id
            ];
            SupplierFillProductCustomerRelationModel::addRelation($relation_insert_data);
            Db::commit();
            $datasmg = ['code'=>200, 'message'=>"添加成功"];
        } catch (\Exception $e) {
            Db::rollback();
            $datasmg = ['code'=>201, 'message'=>$e->getMessage().$e->getLine()];
        }

        return json($datasmg);

    }

    /**
     * @notes  客户报送列表-更新报送
     * @author fengweizhe
     */
    public function updateRequestFill(Request $request) {
        //更新报送 主要是根据关系表中的客户产品id 来更新客户产品信息 以及客户产品信息的碳排放源重写
        $params_payload = $this->req_payload;
        $params_payload_arr = json_decode($params_payload, true);
        $data['id'] = isset($params_payload_arr['id']) ? $params_payload_arr['id'] : '';
        if ($data['id'] == '' || $data['id'] == null || $data['id'] == 0) {
            return json(['code'=>201, 'message'=>"参数id错误"]);
        }
        $data['customer_id'] = isset($params_payload_arr['customer_id']) ? $params_payload_arr['customer_id'] : '';
        if ($data['customer_id'] == '' || $data['customer_id'] == null || $data['customer_id'] == 0) {
            return json(['code'=>201, 'message'=>"参数id错误"]);
        }
        //获取客户产品id
        $customer_product_info = SupplierFillProductCustomerRelationModel::getInfoBySupplierProductIdAndCustomerId($data['id'], $data['customer_id']);
        if (!isset($customer_product_info['customer_product_id'])) {
            return json(['code'=>201, 'message'=>"没有找到对应得客户产品信息"]);
        }
        Db::startTrans();
        try {
            //根据客户产品id更新产品数据
            //查询供应商产品
            $product_info = SupplierProductModel::getDataById($data['id']);
            //整合数据写入产品
            $insert_data['product_name'] = $product_info['product_name'];
            $insert_data['product_model'] = $product_info['product_model'];
            $insert_data['product_type'] = $product_info['product_type'];
            $insert_data['supplier_id'] = $product_info['supplier_id'];
            $insert_data['auth'] = $product_info['auth'];
            $insert_data['state'] = $product_info['state'];
            $insert_data['file_url'] = $product_info['file_url'];
            $insert_data['create_time'] = date('Y-m-d H:i:s');
            $insert_data['modify_time'] = date('Y-m-d H:i:s');
            $insert_data['customer_id'] = $data['customer_id'];
            $insert_data['audit_state'] = $product_info['audit_state'];
            $insert_data['week_start'] = $product_info['week_start'];
            $insert_data['week_end'] = $product_info['week_end'];
            $insert_data['number'] = $product_info['number'];
            $insert_data['unit_type'] = $product_info['customer_id'];
            $insert_data['unit'] = $product_info['unit'];
            $insert_data['emissions'] = $product_info['emissions'];
            $insert_data['coefficient'] = $product_info['coefficient'];
            $data_redis = $request->middleware('data_redis');
            $insert_data['create_by'] = isset($data_redis['userid']) ? $data_redis['userid'] : '';
            $insert_data['modify_by'] = isset($data_redis['userid']) ? $data_redis['userid'] : '';
            $insert_data['id'] = $customer_product_info['customer_product_id'];//编辑客户产品信息,这里要用这个id
            CustomerProductModel::editCustomerProduct($insert_data);
            //删除客户产品源排放源信息
            CustomerProductDischargeModel::delProductDischarge($customer_product_info['customer_product_id']);
            //整合排放源及新增排放源信息
            $discharge_info = SupplierProductDischargeModel::getDataByProductId($data['id']);
            $batch_insert_data = [];
            foreach ($discharge_info as $k => $v) {
                $v_new['type_id'] = $v['type_id'];
                $v_new['type_name'] = $v['type_name'];
                $v_new['name'] = $v['name'];
                $v_new['loss_type'] = $v['loss_type'];
                $v_new['value'] = $v['value'];
                $v_new['unit_type'] = $v['unit_type'];
                $v_new['unit'] = $v['unit'];
                $v_new['factor'] = $v['factor'];
                $v_new['factor_source'] = $v['factor_source'];
                $v_new['unit'] = $v['unit'];
                $v_new['emissions'] = $v['emissions'];
                $v_new['coefficient'] = $v['coefficient'];
                $v_new['customer_product_id'] = $customer_product_info['customer_product_id'];
                $v_new['create_by'] = isset($data_redis['userid']) ? $data_redis['userid'] : '';
                $v_new['modify_by'] = isset($data_redis['userid']) ? $data_redis['userid'] : '';
                $v_new['create_time'] = date('Y-m-d H:i:s');
                $v_new['modify_time'] = date('Y-m-d H:i:s');
                $v_new['is_del'] = SupplierProductDischargeModel::IS_DEL_NOT_DEL;
                $batch_insert_data[$k] = $v_new;
            }
            if (!empty($batch_insert_data)) {
                CustomerProductDischargeModel::addCustomerProductDischarge($batch_insert_data);
            }
            Db::commit();
            $datasmg = ['code'=>200, 'message'=>"更新成功"];
        } catch (\Exception $e) {
            Db::rollback();
            $datasmg = ['code'=>201, 'message'=>$e->getMessage().$e->getLine()];
        }

        return json($datasmg);
    }

    /**
     * @notes  货品报送列表
     * @author fengweizhe
     */
    public function customerRequestFillList() {
//        $supplier_id = isset($_GET['supplier_id']) ? $_GET['supplier_id'] : '';
        $product_name = isset($_GET['product_name']) ? $_GET['product_name'] : '';
        $title = isset($_GET['title']) ? $_GET['title'] : '';
//         if ($supplier_id == '' || $supplier_id == null || $supplier_id == 0) {
//            return json(['code'=>201, 'message'=>"参数supplier_id错误"]);
//        }
        $supplier_id = 1;//todo 从登录态取值
        //这里是查询出客户列表中审核状态为待审核的数据信息  这里的数据  不进行回显 就是那几个字段的信息不进行回写  只推送碳排放信息
        $list = CustomerProductModel::getNotVerifyListBySupplerId($supplier_id, $product_name, $title);
        $data['code'] = 200;
        $data['data']['list'] = $list;

        return json($data);

    }

    /**
     * @notes  附件上传
     * @author fengweizhe
     */
    public function accessory() {

    }

    /**
     * @notes  添加排放源
     * @author fengweizhe
     */
    public function addDischarge() {

    }

    /**
     * @notes  查找排放因子
     * @author fengweizhe
     */
    public function factorList() {

    }

    /**
     * @notes  货品报送列表-查看客户
     * @author fengweizhe
     */
    public function productFillCustomerInfo() {

    }

    /**
     * $notes 货品报送 上报已经存在的商品
     */
    public function subbmitFillExistProduct(Request $request) {
        //前端传客户id和产品的id过来还有客户端的产品id也要传过来 然后自己进行客户产品的相关信息的更新操作 然后产品名称 型号 类型 供应商 创建时间 创建人不改变
        $params_payload = $this->req_payload;
        $params_payload_arr = json_decode($params_payload, true);
        $data['customer_id'] = isset($params_payload_arr['customer_id']) ? $params_payload_arr['customer_id'] : '';
        if ($data['customer_id'] == '' || $data['customer_id'] == null || $data['customer_id'] == 0) {
            return json(['code'=>201, 'message'=>"参数customer_id错误"]);
        }
        $data['customer_product_id'] = isset($params_payload_arr['customer_product_id']) ? $params_payload_arr['customer_product_id'] : '';
        if ($data['customer_product_id'] == '' || $data['customer_product_id'] == null || $data['customer_product_id'] == 0) {
            return json(['code'=>201, 'message'=>"参数customer_product_id错误"]);
        }
        $data['supplier_product_id'] = isset($params_payload_arr['supplier_product_id']) ? $params_payload_arr['supplier_product_id'] : '';
        if ($data['supplier_product_id'] == '' || $data['supplier_product_id'] == null) {
            return json(['code'=>201, 'message'=>"参数supplier_product_id错误"]);
        }
        //查询供应商产品
        $product_info = SupplierProductModel::getDataById($data['supplier_product_id']);

        //整合数据写入产品
        $insert_data['auth'] = $product_info['auth'];
        $insert_data['state'] = $product_info['state'];
        $insert_data['file_url'] = $product_info['file_url'];
        $insert_data['modify_time'] = date('Y-m-d H:i:s');
        $insert_data['audit_state'] = $product_info['audit_state'];
        $insert_data['week_start'] = $product_info['week_start'];
        $insert_data['week_end'] = $product_info['week_end'];
        $insert_data['number'] = $product_info['number'];
        $insert_data['emissions'] = $product_info['emissions'];
        $insert_data['coefficient'] = $product_info['coefficient'];
        $insert_data['unit_type'] = $product_info['customer_id'];
        $insert_data['unit'] = $product_info['unit'];
        $data_redis = $request->middleware('data_redis');
        $insert_data['modify_by'] = isset($data_redis['userid']) ? $data_redis['userid'] : '';
        //根据客户产品id进行更新
        $insert_data['id'] = $data['customer_product_id'];
        CustomerProductModel::editCustomerProduct($insert_data);
        //把客户端产品关联的排放源信息全部删除
        CustomerProductDischargeModel::delProductDischarge($data['customer_product_id']);
        //查询出供应商产品的排放源信息并写入到客户端产品的排放源表中
        $discharge_info = SupplierProductDischargeModel::getDataByProductId($data['supplier_product_id']);
        $batch_insert_data = [];
        foreach ($discharge_info as $k => $v) {
            $v_new['type_id'] = $v['type_id'];
            $v_new['type_name'] = $v['type_name'];
            $v_new['name'] = $v['name'];
            $v_new['loss_type'] = $v['loss_type'];
            $v_new['value'] = $v['value'];
            $v_new['unit_type'] = $v['unit_type'];
            $v_new['unit'] = $v['unit'];
            $v_new['factor'] = $v['factor'];
            $v_new['factor_source'] = $v['factor_source'];
            $v_new['unit'] = $v['unit'];
            $v_new['emissions'] = $v['emissions'];
            $v_new['coefficient'] = $v['coefficient'];
            $v_new['customer_product_id'] = $data['customer_product_id'];
            $v_new['create_by'] = isset($data_redis['userid']) ? $data_redis['userid'] : '';
            $v_new['modify_by'] = isset($data_redis['userid']) ? $data_redis['userid'] : '';
            $v_new['create_time'] = date('Y-m-d H:i:s');
            $v_new['modify_time'] = date('Y-m-d H:i:s');
            $v_new['is_del'] = SupplierProductDischargeModel::IS_DEL_NOT_DEL;
            $batch_insert_data[$k] = $v_new;
        }
        if (!empty($batch_insert_data)) {
            CustomerProductDischargeModel::addCustomerProductDischarge($batch_insert_data);
        }
        $datasmg = ['code'=>200, 'message'=>"上报成功"];
        return json($datasmg);
    }

    /**
     * $notes 货品报送 上报新增的商品
     */
    public function subbmitFillNewAdd(Request $request) {
        if (request()->isPost() && $this->validateForm() === true) {
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);
            $params['customer_id'] = isset($params_payload_arr['customer_id']) ? $params_payload_arr['customer_id'] : '';
            if ($params['customer_id'] == '' || $params['customer_id'] == null || $params['customer_id'] == 0) {
                return json(['code'=>201, 'message'=>"参数customer_id错误"]);
            }
            //先写入供应商方的产品和排放源表
            $data_redis = $request->middleware('data_redis');
            $data['product_name'] = $params_payload_arr['product_name'];
            $data['product_model'] = $params_payload_arr['product_model'];
            $data['product_type'] = $params_payload_arr['product_type'];
            $data['auth'] = $params_payload_arr['auth'];
            $data['week_start'] = $params_payload_arr['week_start'];
            $data['week_end'] = $params_payload_arr['week_end'];
            $data['number'] = $params_payload_arr['number'];
            $data['unit_type'] = $params_payload_arr['unit_type'];
            $data['unit'] = $params_payload_arr['unit'];
            $data['file_url'] = isset($params_payload_arr['file_url']) ? $params_payload_arr['file_url'] : '';
//            $data['supplier_id'] = ''; //todo 供应商id是从客户的登录态中取出客户id查询供应商表 或者 直接把客户id或者供应商id写到缓存中然后直接取都行 这里先空着   supplier_id需要这个字段来做区分是哪个供应商添加的产品
//            $data['customer_id'] = $params_payload_arr['customer_id']; //todo 这里的客户id就先空着  等下会后会有选择的地方
            $data['discharge'] = $params_payload_arr['discharge'];
            $data['create_by'] = isset($data_redis['userid']) ? $data_redis['userid'] : '';
            $data['modify_by'] = isset($data_redis['userid']) ? $data_redis['userid'] : '';
            $data['create_time'] = date('Y-m-d H:i:s');
            $data['modify_time'] = date('Y-m-d H:i:s');
            $discharge = $data['discharge'];
            unset($data['discharge']);

            //封装排放因子数据, 以及产品排放量和排放系数
            foreach ($discharge as $dk => &$dv) {
                //todo 这里排放系数的单位 和 排放量的单位如果不同 需要转换这里 这个逻辑后面再加上
                if ($dv['factor_unit'] != $dv['unit']) {
                    unit_convert($dv['unit'], $dv['factor_unit'], $dv['factor']);
                } else {
                    $dv['emissions'] = $dv['value'] * $dv['factor'];
                }

            }

            //计算出产品的碳排放量
            $emissions_arr = array_column($discharge, 'emissions');
            $product_emissions = array_sum($emissions_arr);
            $data['emissions'] = $product_emissions;
            $data['coefficient'] = $product_emissions / $data['number'];

            //再往客户的表中写一份相同的信息
            $data_customer_insert = $data;
            $data_customer_insert['customer_id'] = $params['customer_id'];
            Db::startTrans();
            try {
                $product_id = SupplierProductModel::addSupplierProduct($data);
                $discharge_insert = [];
                foreach ($discharge as $kk => $vv) {
                    $vv['supplier_product_id'] = $product_id;
                    $discharge_insert[$kk] = $vv;
                }
                SupplierProductDischargeModel::addSupplierProductDischarge($discharge_insert);
                //客户端产品信息写入
                $customer_product_id = CustomerProductModel::addCustomerProduct($data_customer_insert);
                $customer_discharge_insert = [];
                foreach ($discharge as $ck => $cv) {
                    $cv['customer_product_id'] = $customer_product_id;
                    $customer_discharge_insert[$ck] = $cv;
                }
                CustomerProductDischargeModel::addCustomerProductDischarge($customer_discharge_insert);
                Db::commit();
                $datasmg = ['code'=>200, 'message'=>"添加成功"];
            } catch (\Exception $e) {
                Db::rollback();
                $datasmg = ['code'=>201, 'message'=>$e->getMessage().$e->getLine()];
            }
        } else {
            $datasmg = ['code'=>404, 'message'=>$this->validateForm()];
        }

        return json($datasmg);

    }

    public function addFactorForm() {
        $select_list = FactorTypeModel::getList();
        $data['code'] = 200;
        $data['data']['list'] = $select_list;
        return json($data);
    }

    public function addFactorVerify(Request $request) {
//        $params_payload = $this->req_payload;
//        $params_payload_arr = json_decode($params_payload, true);
//        $data['type_id'] = isset($params_payload_arr['type_id']) ? $params_payload_arr['type_id'] : '';
//        if ($data['type_id'] == '' || $data['type_id'] == null || $data['type_id'] == 0) {
//            return json(['code'=>201, 'message'=>"参数type_id错误"]);
//        }
//        $data['name'] = isset($params_payload_arr['name']) ? $params_payload_arr['name'] : '';
//        if ($data['name'] == '' || $data['name'] == null || $data['name'] == 0) {
//            return json(['code'=>201, 'message'=>"参数name错误"]);
//        }
//        $data['loss_type'] = isset($params_payload_arr['loss_type']) ? $params_payload_arr['loss_type'] : '';
//        if ($data['loss_type'] == '' || $data['loss_type'] == null) {
//            return json(['code'=>201, 'message'=>"参数loss_type错误"]);
//        }
//        $data['value'] = isset($params_payload_arr['value']) ? $params_payload_arr['value'] : '';
//        if ($data['value'] == '' || $data['value'] == null) {
//            return json(['code'=>201, 'message'=>"参数value错误"]);
//        }
//        $data['unit_type'] = isset($params_payload_arr['unit_type']) ? $params_payload_arr['unit_type'] : '';
//        if ($data['unit_type'] == '' || $data['unit_type'] == null) {
//            return json(['code'=>201, 'message'=>"参数unit_type错误"]);
//        }
//        $data['unit'] = isset($params_payload_arr['unit']) ? $params_payload_arr['unit'] : '';
//        if ($data['unit'] == '' || $data['unit'] == null) {
//            return json(['code'=>201, 'message'=>"参数unit错误"]);
//        }
//        $data['factor'] = isset($params_payload_arr['factor']) ? $params_payload_arr['factor'] : '';
//        if ($data['factor'] == '' || $data['factor'] == null) {
//            return json(['code'=>201, 'message'=>"参数factor错误"]);
//        }
//        $data['factor_source'] = isset($params_payload_arr['factor_source']) ? $params_payload_arr['factor_source'] : '';
//        if ($data['factor_source'] == '' || $data['factor_source'] == null) {
//            return json(['code'=>201, 'message'=>"参数factor_source错误"]);
//        }
//        $data['factor_unit'] = isset($params_payload_arr['factor_unit']) ? $params_payload_arr['factor_unit'] : '';
//        if ($data['factor_unit'] == '' || $data['factor_unit'] == null) {
//            return json(['code'=>201, 'message'=>"参数factor_unit错误"]);
//        }
        //todo  这里先直接返回验证成功  错误的话  message要给  成功的话 message为空就行了
        $datasmg = ['code'=>200, 'message'=>""];
        return json($datasmg);
    }

    /**
     * validateForm 验证
     *
     * @return void
     */
    protected function validateForm() {
        $data = request()->param();

        try {
            validate(SupplierProductValidate::class)->check($data);

            return  true;
        } catch (ValidateException $e) {

            return $e->getError();
        }
    }
}