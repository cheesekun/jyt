<?php


namespace app\api\controller\supply;


use app\BaseController;
use app\model\admin\IndustryModel;
use app\model\supply\CustomerModel;
use app\model\supply\SupplierModel;
use think\Request;

class Supplier extends BaseController
{
    /**
     * 我是供应商 查看的是采购者的信息
     */

    /**
     * @notes 供应商客户的列表
     * @author fengweizhe
     */
    public function index() {
        $title = isset($_GET['title']) ? $_GET['title'] : '';
        $state = isset($_GET['state']) ? $_GET['state'] : '';
        $industry_id = isset($_GET['industry_id']) ? $_GET['industry_id'] : '';
        $page_size = isset($_GET['pageSize']) ? $_GET['pageSize'] : CustomerModel::pageSize;
        $page_index = isset($_GET['pageIndex']) ? $_GET['pageIndex'] : CustomerModel::pageIndex;
        $main_id = 1; //todo 这里后面要从 session中获取
        $list = CustomerModel::getList($page_size, $page_index, ['title'=>$title, 'state'=>$state,
            'industry_id'=>$industry_id, 'main_id'=>$main_id])->toArray();
        $industry_id_map = IndustryModel::getList();
        $state_select_map = CustomerModel::STATE_SELECT_MAP;
        $state_map = CustomerModel::STATE_MAP;

        $list_new = [];
        foreach ($list['data'] as $k => $v) {
            if (isset($state_map[$v['state']])) {
                $v['state_name'] = $state_map[$v['state']];
            } else {
                $v['state_name'] = '';
            }
            $list_new[$k] = $v;
        }
        unset($list['data']);

        $data['code'] = 200;
        $data['data']['list'] = $list_new;
        $data['data']['industry_id'] = $industry_id_map;
        $data['data']['state'] = $state_select_map;
        $data['data']['total'] = $list['total'];
        return json($data);
    }

    /**
     * @notes 供应商客户的详情
     * @author fengweizhe
     */
    public function info() {
        $id = isset($_GET['id']) ? $_GET['id'] : '';
        $info = CustomerModel::getDataById($id);
        if ($info == null) {
            return json(['code'=>201, 'message'=>"未查询到客户供应商相关信息"]);
        }
        $data['code'] = 200;
        $data['data'] = $info;

        return json($data);
    }

    /**
     * @notes 供应商客户的启用/禁用
     * @author fengweizhe
     */
    public function state(Request $request) {
        $params_payload = $this->req_payload;
        $params_payload_arr = json_decode($params_payload, true);
        $data['id'] = isset($params_payload_arr['id']) ? $params_payload_arr['id'] : '';
        if ($data['id'] == '' || $data['id'] == null || $data['id'] == 0) {
            return json(['code'=>201, 'message'=>"参数id错误"]);
        }
        $data['state'] = isset($params_payload_arr['state']) ? $params_payload_arr['state'] : '';
        if ($data['state'] == '' || $data['state'] == null || $data['state'] == 0) {
            return json(['code'=>201, 'message'=>"参数state错误"]);
        }
        if (!in_array($data['state'], [CustomerModel::STATE_YES, CustomerModel::STATE_NO])) {
            return json(['code'=>201, 'message'=>"参数state错误"]);
        }
        CustomerModel::updateState($data['id'], $data['state']);
        if ($data['state'] == SupplierModel::STATE_YES) {
            return json(['code'=>200, 'message'=>"启用成功"]);
        } else {
            return json(['code'=>200, 'message'=>"禁用成功"]);
        }
    }

}