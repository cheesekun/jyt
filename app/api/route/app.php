<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\facade\Route;

// 产品碳足迹碳标签管理列表
Route::get('productcarbonlabel', 'product.ProductCarbonLabel/index');
// 产品碳足迹碳标签 获取新增表单接口
Route::get('productcarbonlabeladdform', 'product.ProductCarbonLabel/addform');
// 产品碳足迹碳标签 新增接口
Route::post('productcarbonlabeladd', 'product.ProductCarbonLabel/add');
// 产品碳足迹碳标签 新增接口
Route::post('productcarbonlabeledit', 'product.ProductCarbonLabel/edit');
// 产品碳足迹碳标签 删除接口
Route::post('productcarbonlabeldel', 'product.ProductCarbonLabel/del');
// 产品碳足迹碳标签 详情接口
Route::get('productcarbonlabelinfo', 'product.ProductCarbonLabel/info');
// 产品碳足迹碳标签 获取二维码图片接口
Route::get('productcarbonlabelqrcodeurl', 'product.ProductCarbonLabel/getqrcodeurl');

//供应链-我是采购端-供应商列表
Route::get('supplycustomer', 'supply.Customer/index');
//供应链-我是采购端-供应商详情
Route::get('supplycustomerinfo', 'supply.Customer/info');
//供应链-我是采购端-供应商禁用/启用
Route::post('supplycustomerstate', 'supply.Customer/state');
//供应链-我是采购端-供应商风险状态调整表单
Route::get('supplycustomerriskstatemap', 'supply.Customer/riskstatemap');
//供应链-我是采购端-供应商风险状态调整
Route::post('supplycustomerriskstate', 'supply.Customer/riskstate');
//供应链-我是采购端-供应产品管理列表
Route::get('supplycustomerproduct', 'supply.CustomerProduct/index');
//供应链-我是采购端-供应产品新增
Route::post('supplycustomerproductadd', 'supply.CustomerProduct/add');
//供应链-我是供应端-产品新增表单信息
Route::get('supplycustomerproductaddform', 'supply.CustomerProduct/addForm');
//供应链-我是采购端-供应产品编辑
Route::post('supplycustomerproductedit', 'supply.CustomerProduct/edit');
//供应链-我是采购端-供应产品删除
Route::post('supplycustomerproductdel', 'supply.CustomerProduct/del');
//供应链-我是采购端-供应产品详情
Route::get('supplycustomerproductinfo', 'supply.CustomerProduct/info');
//供应链-我是采购端-供应产品启用/禁用
Route::post('supplycustomerproductstate', 'supply.CustomerProduct/state');
//供应链-我是采购端-供应产品要求填报
Route::post('supplycustomerproductrequestfill', 'supply.CustomerProduct/requestfill');
//供应链-我是采购端-供应产品审核/驳回
Route::post('supplycustomerproductauditstate', 'supply.CustomerProduct/auditstate');
//供应链-我是采购端-供应产品报送审批列表
Route::get('supplycustomerproductauditlist', 'supply.CustomerProduct/auditlist');

//供应链-我是供应端-客户列表
Route::get('supplysupplier', 'supply.Supplier/index');
//供应链-我是供应端-客户详情
Route::get('supplysupplierinfo', 'supply.Supplier/info');
//供应链-我是供应端-客户列表
Route::post('supplysupplierstate', 'supply.Supplier/state');
//供应链-我是供应端-产品列表
Route::get('supplysupplierproduct', 'supply.SupplierProduct/index');
//供应链-我是供应端-产品新增
Route::post('supplysupplierproductadd', 'supply.SupplierProduct/add');
//供应链-我是供应端-产品新增表单信息
Route::get('supplysupplierproductaddform', 'supply.SupplierProduct/addForm');
//供应链-我是供应端-供应产品编辑
Route::post('supplysupplierproductedit', 'supply.SupplierProduct/edit');
//供应链-我是供应端-供应产品删除
Route::post('supplysupplierproductdel', 'supply.SupplierProduct/del');
//供应链-我是供应端-供应产品详情
Route::get('supplysupplierproductinfo', 'supply.SupplierProduct/info');
//供应链-我是供应端-供应产品管理-客户报送
Route::get('supplysupplierproductfilllist', 'supply.SupplierProduct/supplierProductFillList');
//供应链-我是供应端-供应产品管理-发起报送
Route::post('supplysupplierproductrequestfill', 'supply.SupplierProduct/RequestFill');
//供应链-我是供应端-供应产品管理-更新报送
Route::post('supplysupplierproductupdaterequestfill', 'supply.SupplierProduct/UpdateRequestFill');
//供应链-我是供应端-货品报送-进行报送列表
Route::get('supplysupplierproductcustomerrequestfilllist', 'supply.SupplierProduct/CustomerRequestFillList');
//供应链-我是供应端-货品报送-进行报送选择已有
Route::post('supplysupplierproductsubbmitfillexistproduct', 'supply.SupplierProduct/SubbmitFillExistProduct');
//供应链-我是供应端-货品报送-进行报送新增产品
Route::post('supplysupplierproductsubbmitfillnewadd', 'supply.SupplierProduct/SubbmitFillNewAdd');
//供应链-我是供应端-供应产品管理-添加排放源表单获取
Route::get('supplysupplieraddfactorform', 'supply.SupplierProduct/AddFactorForm');
//供应链-我是供应端-供应产品管理-添加排放源验证接口
Route::post('supplysupplieraddfactorvarify', 'supply.SupplierProduct/AddFactorVerify');