<?php
declare (strict_types = 1);

namespace app\middleware;

use think\cache\driver\Redis;

class Check
{
    /**
     * 处理请求
     *
     * @param \think\Request $request
     * @param \Closure       $next
     * @return Response
     */

    public function handle($request, \Closure $next)
    {
        $token = $request->header('token');
        if($token != 'null' && $token != null) {
            $redis = new Redis();
            $data_redis = $redis->get($token);
            $data_redis = json_decode($data_redis, true);
            $request->withMiddleware(['data_redis' => $data_redis]);
        }
        
        return $next($request);
    }
}
